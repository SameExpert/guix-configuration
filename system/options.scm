;; This file should define these variables:
;; * use-nonfree-packages?
;; * use-nonfree-nvidia-driver?
;; * my-host-name
;; * my-timezone
;; * my-locale
;; * my-keyboard-layout
;; * my-name
;; * my-username
;; * my-desktop-environment

(define-module (options))

(define-public use-nonfree-packages?
 ;; A boolean.
 ;; Do you want to use nonfree packages?
 #f)

(define-public use-nonfree-nvidia-driver?
 ;; A boolean.
 ;; Do you want to use nonfree NVIDIA driver?
 #f)

(define-public my-host-name
 ;; A string.
 "my_host_name")

(define-public my-timezone
 ;; A string.
 "Europe/Berlin")

(define-public my-locale
 ;; A string.
 "en_US.utf8")

(define-public my-keyboard-layout
 ;; A list of one or two strings containing a keyboard layout and optional
 ;; variant respectively.
 ;; Refer to LAYOUTS section of XKEYBOARD-CONFIG(7) man page:
 ;; `man -P 'less -p ^LAYOUTS' xkeyboard-config.7`
 (list "us"))

(define-public my-name
 ;; A string containing your name.
 "Sughosha")

(define-public my-username
 ;; A string containing a username.
 "sughosha")

(define-public my-desktop-environment
 ;; "gnome", "lxqt", "plasma", "sway" or "minimal".
 "plasma")
