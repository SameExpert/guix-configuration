(define-module (functions)
  #:use-module (guix gexp))

(define-public (fix-file-name name)
  (if (string-index name #\/)
    (if
      (string-index
        (substring
          name
          (+ (string-index-right name #\/) 1))
        #\.)
      (if
        (=
          (string-index
            (substring
              name
              (+ (string-index-right name #\/) 1))
            #\.)
          0)
        (substring
          name
          (+ (string-index-right name #\/) 2))
        (substring
          name
          (+ (string-index-right name #\/) 1)))
      (substring
        name
        (+ (string-index-right name #\/) 1)))
    (if (string-index name #\.)
      (if (= (string-index name #\.) 0)
        (string-replace name "" 0 1)
        name)
      name)))

(define-public (skeleton-file-name name)
  (string-append "skeleton-" (fix-file-name name)))

(define-public (skeleton-file-path name)
  (local-file
    (assume-source-relative-file-name
     (string-append "files/skel/" name))
    (skeleton-file-name name)
    #:recursive? #t))
