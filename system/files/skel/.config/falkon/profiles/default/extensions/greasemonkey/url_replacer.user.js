// ==UserScript==
// @name        url replacer
// @namespace   global
// @description replace peace of string
// @include     http*://*.youtube.com/*
// @include     http*://*wikipedia.org*
// @version     1
// @grant       none
// @run-at      document-start
// ==/UserScript==
// desc: replace http://www.youtube.com/v/CZWlWXUaTwI to http://www.youtube.com/watch?v=CZWlWXUaTwI

var url = location.href;

if (location.href.search("youtube.com")>0) {
  url = url.replace("youtube.com","invidious.no-logs.com");
  location.href=url;
}

if (location.href.search("wikipedia.org")>0) {
  url = url.replace("*wikipedia.org","https://wikiless.org");
  location.href=url;
}
