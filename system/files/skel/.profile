if [ -f /etc/environment ]
	then
		. /etc/environment
		export $(cat /etc/environment | cut -d= -f1)
fi
for i in $HOME/.guix-extra-profiles/*/profile; do
	if [ -f $i/etc/profile ]
		then
			. $i/etc/profile
			export PATH=$PATH${PATH:+:}"$i/bin"
			export GST_PLUGIN_PATH="$GST_PLUGIN_PATH${GST_PLUGIN_PATH:+:}$i/lib/gstreamer-1.0"
			export XDG_CONFIG_DIRS="$XDG_CONFIG_DIRS${XDG_CONFIG_DIRS:+:}$i/etc"
			export XDG_DATA_DIRS="$XDG_DATA_DIRS${XDG_DATA_DIRS:+:}$i/share"
			export MANPATH="$MANPATH${MANPATH:+:}$i/share/man"
			export INFOPATH="$INFOPATH${INFOPATH:+:}$i/share/info"
			export DICPATH="$DICPATH${DICPATH:+:}$i/share/hunspell"
    fi
done
export PATH="$HOME/.local/bin${PATH:+:}$PATH"
export XDG_DATA_DIRS="$XDG_DATA_DIRS${XDG_DATA_DIRS:+:}$HOME/.local/share/flatpak/exports/share:/var/lib/flatpak/exports/share"
export MANPATH="$XDG_DATA_HOME/man$MANPATH${MANPATH:+:}"
export INFOPATH="$XDG_DATA_HOME/info$INFOPATH${INFOPATH:+:}"
export DICPATH="$XDG_DATA_HOME/hunspell$DICPATH${DICPATH:+:}"
export SF2_PATH="$XDG_DATA_HOME/sf2${SF2_PATH:+:}$SF2_PATH"
export SFZ_PATH="$XDG_DATA_HOME/sfz${SFZ_PATH:+:}$SFZ_PATH"
export CLAP_PATH="$HOME/.local/lib/clap${CLAP_PATH:+:}$CLAP_PATH"
export LADSPA_PATH="$HOME/.local/lib/ladspa${LADSPA_PATH:+:}$LADSPA_PATH"
export LV2_PATH="$HOME/.local/lib/lv2${LV2_PATH:+:}$LV2_PATH"
export LXVST_PATH="$HOME/.local/lib/lxvst${LXVST_PATH:+:}$LXVST_PATH"
export VST2_PATH="$HOME/.local/lib/vst${VST2_PATH:+:}$VST2_PATH"
export VST3_PATH="$HOME/.local/lib/vst3${VST3_PATH:+:}$VST3_PATH"
