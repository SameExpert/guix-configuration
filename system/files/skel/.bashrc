export SHELL

if [[ $- != *i* ]]
  then
    [[ -n "$SSH_CLIENT" ]] && source /etc/profile
    return
fi

[ -f /etc/bashrc ] && source /etc/bashrc

export PS1="\[\033[;32m\]┌──(\[\033[1;34m\]\u@\h\[\033[;32m\])-[\[\033[0;1m\]\w\[\033[;32m\]]${GUIX_ENVIRONMENT:+[env]}\n\[\033[;32m\]└─\[\033[1;34m\]\$\[\033[0m\] "
