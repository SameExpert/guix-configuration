;; Load these variables:
;; * use-nonfree-packages?
;; * use-nonfree-nvidia-driver?
;; * my-host-name
;; * my-timezone
;; * my-locale
;; * my-keyboard-layout
;; * my-name
;; * my-username
;; * my-desktop-environment
(load "options.scm")

;; Load these functions:
;; * fix-file-name
;; * skeleton-file-name
;; * skeleton-file-path
(load "functions.scm")

(define-module (operating-systems)
  #:use-module (gnu)
  #:use-module (gnu system)
  #:use-module (gnu system file-systems)
  #:use-module (gnu system keyboard)
  #:use-module (gnu system nss)
  #:use-module (gnu system shadow)
  #:use-module (nongnu system linux-initrd)
  #:use-module (gnu services avahi)
  #:use-module (gnu services dbus)
  #:use-module (gnu services desktop)
  #:use-module (gnu services guix)
  #:use-module (gnu services networking)
  #:use-module (gnu services pm)
  #:use-module (gnu services sddm)
  #:use-module (gnu services sound)
  #:use-module (gnu services xorg)
  #:use-module (gnu services virtualization)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages package-management)
  #:use-module (nongnu packages linux)
  #:use-module (nongnu packages nvidia)
  #:use-module (nongnu services nvidia)
  #:use-module (guix channels)
  #:use-module (guix gexp)
  #:use-module (guix transformations)
  #:use-module (functions)
  #:use-module (kde-xyz)
  #:use-module (options))

(define-public %system-channels
 (append
   (if use-nonfree-packages?
     (list
       (channel
         (name 'nonguix)
         (url "https://gitlab.com/nonguix/nonguix")
         ;; Enable signature verification:
         (introduction
          (make-channel-introduction "897c1a470da759236cc11798f4e0a5f7d4d59fbc"
           (openpgp-fingerprint
            "2A39 3FFF 68F4 EF7A 3D29  12AF 6F51 20A0 22FB B2D5")))))
       '())
     %default-channels))

(define-public %system-keyboard-layout
  (keyboard-layout
    (list-ref my-keyboard-layout 0)
    (if (= (length my-keyboard-layout) 2)
      (list-ref my-keyboard-layout 1)
      "")
    #:options (list "grp:win_space_toggle,compose:caps")))

(define-public %system-xorg-config
 (if (and use-nonfree-packages? use-nonfree-nvidia-driver?)
   (xorg-configuration
     (modules (cons* nvidia-driver %default-xorg-modules))
     (server (replace-mesa (specification->package+output "xorg-server")))
     (drivers '("nvidia"))
     (keyboard-layout %system-keyboard-layout))
   (xorg-configuration
     (keyboard-layout %system-keyboard-layout))))

(define-public %system-skeletons
 `(("skel"
    ,(skeleton-directory
      (map
        (lambda (file-name)
          `(,file-name
            ,(skeleton-file-path file-name)))
        (list
          ".bashrc"
          ".config/fontconfig"
          ".config/gdb"
          ".config/nano"
          ".config/wgetrc"
          ".guile"
          ".local/share/templates/guix-package.desktop"
          ".local/share/templates/hello.scm"
          ".local/share/templates/shell-script.desktop"
          ".local/share/templates/shell-script.sh"
          ".profile"
          ".Xdefaults"
          ".zprofile"))))))

(define-public %system-packages
 (append
   (map specification->package+output
    (list
      "alsa-utils"
      "btrfs-progs"
      "font-google-noto"
      "font-google-noto-emoji"
      "font-google-noto-sans-cjk"
      "fuse"
      "glibc-locales"
      "graphviz"
      "hicolor-icon-theme"
      "man-pages"
      "ncurses"
      "ntfs-3g"
      "setxkbmap"
      "unicode-emoji"
      "util-linux"
      "xkeyboard-config"))
   (if (and use-nonfree-packages? use-nonfree-nvidia-driver?)
     (map specification->package+output
      (list
        "nvidia-driver"
        "nvidia-exec"
        "nvidia-settings"))
     '())
   (if (and use-nonfree-packages? use-nonfree-nvidia-driver?)
     (list (replace-mesa (specification->package+output "wine64")))
     (list (specification->package+output "wine64")))
   %base-packages))

(define-public %system-services
 (append
   (modify-services %base-services
     (guix-service-type config =>
      (guix-configuration
        (inherit config)
        (substitute-urls
         (append
           %default-substitute-urls
           '("https://berlin.guix.gnu.org")
           '("https://ci.guix.info")
           '("https://substitutes.nonguix.org")))
        (authorized-keys
         (append
           %default-authorized-guix-keys
           `(,(file-append guix "/share/guix/berlin.guix.gnu.org.pub"))
           `(,(file-append guix "/share/guix/ci.guix.info.pub"))
           `(,(local-file "files/guix/substitutes.nonguix.org.pub"))))
        (channels %system-channels))))
   (list
     fontconfig-file-system-service
     polkit-wheel-service
     (service accountsservice-service-type)
     (service alsa-service-type
      (alsa-configuration
        (pulseaudio? #f)))
     (service avahi-service-type)
     (service bluetooth-service-type
      (bluetooth-configuration
        (name my-host-name)))
     (service colord-service-type)
     (service dbus-root-service-type)
     (service dhcp-client-service-type)
     (service elogind-service-type)
     (service guix-home-service-type
      `((,my-username ,(load "../home/home-environment.local.scm"))))
     (service libvirt-service-type
      (libvirt-configuration (unix-sock-group "libvirt")))
     (service ntp-service-type)
     (service polkit-service-type)
     (service udisks-service-type)
     (service upower-service-type)
     (service usb-modeswitch-service-type)
     (service virtlog-service-type)
     (service wpa-supplicant-service-type)
     (service x11-socket-directory-service-type)
     (udev-rules-service 'extra-udev-rules
      (map specification->package+output (list "libmtp"))))
   (if (and use-nonfree-packages? use-nonfree-nvidia-driver?)
    (list
      (service nvidia-service-type)
      (simple-service 'nvidia-env session-environment-service-type
       `(("__EGL_VENDOR_LIBRARY_FILENAMES" .
          (string-append
            "/run/current-system/profile/share/glvnd"
            "/egl_vendor.d/50_mesa.x86_64.json"))
         ("__GLX_VENDOR_LIBRARY_NAME" . "nvidia")
         ("__NV_PRIME_RENDER_OFFLOAD" . "1")
         ("VK_DRIVER_FILES" .
          (string-append
            "/run/current-system/profile/share/vulkan"
            "/icd.d/nvidia_icd.x86_64.json"))
         ("__VK_LAYER_NV_optimus" . "NVIDIA_only"))))
    '())))

(define-public base-desktop
 (operating-system
   (kernel
    (if use-nonfree-packages?
      (if use-nonfree-nvidia-driver? linux-lts linux)
      linux-libre))
   (kernel-arguments
    (if (and use-nonfree-packages? use-nonfree-nvidia-driver?)
      (append
        '("modprobe.blacklist=nouveau"
          "nvidia_drm.modeset=1")
        %default-kernel-arguments)
      %default-kernel-arguments))
   (kernel-loadable-modules
    (if (and use-nonfree-packages? use-nonfree-nvidia-driver?)
      (list nvidia-module)
      '()))
   (initrd (if use-nonfree-packages? microcode-initrd base-initrd))
   (initrd-modules (append (list "btrfs" "msdos") %base-initrd-modules))
   (firmware
    (cons* (if use-nonfree-packages? (list linux-firmware) %base-firmware)))
   (host-name my-host-name)
   (timezone my-timezone)
   (locale my-locale)
   (keyboard-layout %system-keyboard-layout)
   (bootloader
    (bootloader-configuration
      (bootloader grub-efi-bootloader)
      (targets '("/boot/efi"))
      (keyboard-layout keyboard-layout)))
   (file-systems
    (append
      (list
        (file-system
          (device (file-system-label "GNU-Guix"))
          (mount-point "/")
          (type "btrfs"))
        (file-system
          (device (file-system-label "GNU-ESP"))
          (mount-point "/boot/efi")
          (type "vfat")))
     %base-file-systems))
   (users
    (append
      (list
        (user-account
          (name my-username)
          (comment my-name)
          (password (crypt my-username "$6$abc"))
          (group "users")
          (supplementary-groups
           '("audio" "kvm" "libvirt" "lp" "video" "wheel"))))
      %base-user-accounts))
   (skeletons %system-skeletons)
   (packages %system-packages)
   (services %system-services)
   (name-service-switch %mdns-host-lookup-nss)))

(define-public gnome-desktop
 (operating-system (inherit base-desktop)
   (packages
    (append
      (map specification->package+output
       (list
         "chess"
         "dconf-editor"
         "gcolor3"
         "gitg"
         "gnome-chess"
         "secrets"
         "transmission:gui"))
      %system-packages))
   (services
    (append
      (list
        (service gdm-service-type
         (gdm-configuration
           (gnome-shell-assets
             (map specification->package+output
              (list
                "adwaita-icon-theme"
                "deja-dup"
                "geary"
                "evolution-data-server"
                "garuda-backgrounds"
                "gnome-shell-extension-blur-my-shell"
                "gnome-shell-extension-burn-my-windows"
                "gnome-shell-extension-clipboard-indicator"
                "gnome-shell-extension-just-perfection"
                "gnome-shell-extension-noannoyance"
                "gnome-shell-extension-paperwm"
                "gnome-shell-extension-radio"
                "gnome-shell-extension-unite-shell"
                "gnome-shell-extension-v-shell"
                "gsettings-desktop-schemas"
                "gst-libav"
                "gst-plugins-bad"
                "gst-plugins-good"
                "gst-plugins-ugly"
                "libmatroska"
                "polari"
                "polkit-gnome"
                "seahorse"
                "sweet-nova-theme"
                "tracker"
                "tracker-miners"
                "usbguard"
                "xdg-desktop-portal-gtk"
                "xprop")))))
        (service gnome-desktop-service-type)
        (service gnome-keyring-service-type)
        (service network-manager-service-type
         (network-manager-configuration
           (vpn-plugins
            (map specification->package+output
             (list "network-manager-openvpn")))))
        (service pam-limits-service-type
         (list
           (pam-limits-entry "@audio" 'both 'rtprio 99)
           (pam-limits-entry "@audio" 'both 'memlock 'unlimited)))
        (simple-service 'gnome-env session-environment-service-type
         `(("GTK_THEME" . "Sweet"))))
      (list (set-xorg-configuration %system-xorg-config))
      (modify-services %system-services
        (delete dhcp-client-service-type))))))

(define-public lxqt-desktop
 (operating-system (inherit base-desktop)
   (packages
    (append
      (map specification->package+output
       (list
         "lxqt-archiver"
         "lxqt-openssh-askpass"
         "screengrab"))
      %system-packages))
   (services
    (append
      (list
        (service lxqt-desktop-service-type)
        (service network-manager-service-type
         (network-manager-configuration
           (vpn-plugins
            (map specification->package+output
             (list "network-manager-openvpn")))))
        (service pam-limits-service-type
         (list
           (pam-limits-entry "@audio" 'both 'rtprio 99)
           (pam-limits-entry "@audio" 'both 'memlock 'unlimited)))
        (service sddm-service-type
         (sddm-configuration
           (display-server "wayland"))))
      (modify-services %system-services
        (delete dhcp-client-service-type))))))

(define-public dr460nized-plasma-desktop
 (operating-system (inherit base-desktop)
   (keyboard-layout %system-keyboard-layout)
   (bootloader
    (bootloader-configuration
      (bootloader grub-efi-bootloader)
      (targets '("/boot/efi"))
      (keyboard-layout keyboard-layout)
      (theme
       (grub-theme
         (image
          (file-append garuda-wallpapers
           "/share/wallpapers/garuda-wallpapers/Dragon.png"))
         (resolution '(1920 . 1080))))))
   (skeletons
    (append
      `(("skel-dr460nized-kde"
         ,(skeleton-directory
           (map
             (lambda (file-name)
               `(,file-name
                 ,(skeleton-file-path file-name)))
             (list
               ".config/autostart/jdsp-gui.desktop"
               ".config/autostart/org.kde.kget.desktop"
               ".config/autostart/org.kde.yakuake.desktop"
               ".config/arkrc"
               ".config/baloofilerc"
               ".config/dolphinrc"
               ".config/falkon"
               ".config/gtk-3.0"
               ".config/gtk-4.0"
               ".config/katerc"
               ".config/kcminputrc"
               ".config/kdedefaults"
               ".config/kdeglobals"
               ".config/konsolerc"
               ".config/kscreenlockerrc"
               ".config/ksplashrc"
               ".config/Kvantum"
               ".config/kwinrc"
               ".config/libinput-gestures.conf"
               ".config/plasmarc"
               ".config/plasma-org.kde.plasma.desktop-appletsrc"
               ".config/touchpadrc"
               ".config/yakuakerc"
               ".local/share/org.kde.syntax-highlighting")))))
      %system-skeletons))
   (packages
    (append
      (let
        ((pkglist
          (list
           "akonadi"
           "akonadi-calendar"
           "akonadi-contacts"
           "akonadi-mime"
           "akonadi-notes"
           "akonadi-search"
           "akregator"
           "amarok"
           "ark"
           "bluez"
           "bluez-alsa"
           "bluedevil"
           "bluez-qt"
           "dolphin-plugins"
           "dragon"
           "elisa"
           "encfs"
           "falkon"
           "filelight"
           "gwenview"
           "isoimagewriter"
           "jamesdsp"
           "kaccounts-providers"
           "kaddressbook"
           "kate"
           "kbackup"
           "kcalc"
           "kcharselect"
           "kcolorchooser"
           "kded"
           "kdepim-runtime"
           "kfind"
           "kget"
           "kgpg"
           "khelpcenter"
           "kio-extras"
           "kio-fuse"
           "kleopatra"
           "kmail"
           "kmail-account-wizard"
           "kommit"
           "kompare"
           "konversation"
           "korganizer"
           "krdc"
           "krename"
           "krfb"
           "ktimer"
           "ktorrent"
           "kwalletmanager"
           "okular"
           "partitionmanager"
           "pinentry-qt"
           "plasma-pass"
           "ruqola"
           "signond"
           "signon-plugin-oauth2"
           "sweeper"
           "yakuake")))
        (if (and use-nonfree-packages? use-nonfree-nvidia-driver?)
          (map
            (lambda (pkg)
              (replace-mesa (specification->package+output pkg)))
            pkglist)
          (map specification->package+output pkglist)))
      (let
        ((pkglist
          (list garuda-dr460nized-kde-theme)))
        (if (and use-nonfree-packages? use-nonfree-nvidia-driver?)
          (map
            (lambda (pkg)
              (replace-mesa pkg))
            pkglist)
          pkglist))
      %system-packages))
   (services
    (append
      (list
        (service network-manager-service-type
         (network-manager-configuration
           (vpn-plugins
            (map specification->package+output
             (list "network-manager-openvpn")))))
        (service pam-limits-service-type
         (list
           (pam-limits-entry "@audio" 'both 'rtprio 99)
           (pam-limits-entry "@audio" 'both 'memlock 'unlimited)))
        (service plasma-desktop-service-type
         (plasma-desktop-configuration
           (plasma-package
            (if (and use-nonfree-packages? use-nonfree-nvidia-driver?)
              (replace-mesa (specification->package+output "plasma"))
              (specification->package+output "plasma")))))
        (service power-profiles-daemon-service-type)
        (service sddm-service-type
         (sddm-configuration
           (sddm
            (if (and use-nonfree-packages? use-nonfree-nvidia-driver?)
              (replace-mesa (specification->package+output "sddm"))
              (specification->package+output "sddm")))
           ;(display-server "wayland")
           (theme "Dr460nized")
           (xorg-configuration %system-xorg-config)))
        (simple-service 'kde-env session-environment-service-type
         `(("GIT_ASKPASS" . "ksshaskpass")
           ("GTK_THEME" . "Sweet")
           ("QTWEBENGINE_CHROMIUM_FLAGS" .
            "--blink-settings=forceDarkModeEnabled=true,--force-dark-mode")))
        (simple-service 'kde-bus dbus-root-service-type
          (let
            ((pkglist
              (list
                "kcron"
                "kdeplasma-addons"
                "kglobalaccel"
                "kpmcore"
                "kwalletmanager"
                "packagekit")))
            (if (and use-nonfree-packages? use-nonfree-nvidia-driver?)
              (map
               (lambda (pkg)
                 (replace-mesa (specification->package+output pkg)))
               pkglist)
              (map specification->package+output pkglist))))
        (simple-service 'kde-policy-kit polkit-service-type
          (let
            ((pkglist
              (list
                "kcron"
                "kdeplasma-addons"
                "kpmcore"
                "kwalletmanager"
                "packagekit")))
            (if (and use-nonfree-packages? use-nonfree-nvidia-driver?)
              (map
               (lambda (pkg)
                 (replace-mesa (specification->package+output pkg)))
               pkglist)
              (map specification->package+output pkglist)))))
      (modify-services %system-services
        (delete dhcp-client-service-type))))))

(define-public sway-desktop
 (operating-system (inherit base-desktop)
   (kernel linux)
   (kernel-arguments %default-kernel-arguments)
   (kernel-loadable-modules '())
   (skeletons
    (append
      `(("skel-sway"
         ,(skeleton-directory
           (map
             (lambda (file-name)
               `(,file-name
                 ,(skeleton-file-path file-name)))
             (list ".config/sway")))))
      %system-skeletons))
   (packages
    (append
      (map specification->package+output
       (list
         "alacritty"
         "avizo"
         "curseradio"
         "fuzzel"
         "htop"
         "icecat"
         "icedove"
         "imv"
         "light"
         "lynx"
         "mpv"
         "musikcube"
         "pulseaudio"
         "pamixer"
         "ranger"
         "swappy"
         "sway"
         "swaybg"
         "swayhide"
         "swayidle"
         "swaylock"
         "swayr"
         "waybar"
         "wl-clipboard"
         "wf-recorder"
         "wob"
         "xdg-utils"
         "xdg-desktop-portal-wlr"
         "zathura"
         "zathura-pdf-poppler"))
      %system-packages))
   (services
    (append
      (list
        (service greetd-service-type
         (greetd-configuration
           (greeter-supplementary-groups '("audio" "input" "video"))
           (terminals
            (list
              (greetd-terminal-configuration
                (terminal-vt "7")
                (terminal-switch #t)
                (default-session-command
                 (greetd-wlgreet-sway-session)))))))
        (service screen-locker-service-type
         (screen-locker-configuration
           (name "swaylock")
           (program
            (file-append
              (specification->package+output "swaylock-effects")
              "/bin/swaylock"))
           (allow-empty-password? #t)))
        (udev-rules-service 'sway-udev-rules
         (map specification->package+output (list "light"))))
      (if (and use-nonfree-packages? use-nonfree-nvidia-driver?)
        (modify-services %system-services
          (delete nvidia-service-type))
        %system-services)))))
