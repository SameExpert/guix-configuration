;;; GNU Guix --- Functional package management for GNU
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(load "operating-systems.scm")
(load "options.scm")

(use-modules
  (gnu)
  (gnu image)
  (gnu system)
  (gnu system file-systems)
  (gnu system image)
  (guix gexp)
  (ice-9 match)
  (operating-systems)
  (options))

(image
  (name 'same-expert-guix-desktop)
  (format 'iso9660)
  (operating-system
   (operating-system
     (inherit
      (match my-desktop-environment
        ("gnome" gnome-desktop)
        ("lxqt" lxqt-desktop)
        ("plasma" dr460nized-plasma-desktop)
        ("sway" sway-desktop)
        ("minimal" base-desktop)))
     (file-systems '())))
  (partitions
   (list
     (partition
       (file-system "btrfs")
       (label "GNU-Guix")
       (flags '(boot))
       (initializer (gexp initialize-root-partition))))))
