## Description
My [Guix](https://guix.gnu.org) system setup configuration.
I used this for my Omen by HP Laptop. May or may not work with other machines. You may have to edit some files according to your needs.

**NOTE: Refer to [GNU Guix Reference Manual](https://guix.gnu.org/manual/). This is intended to be used by a user with at least basic knowledge of how Guix works. I don't hold any responsibility for anything that can happen to your device or files.**

## Installation
These configurations look for packages defined in *packages/* folder. Set your `GUIX_PACKAGE_PATH` environment variable:
```
export GUIX_PACKAGE_PATH=packages
```

### System configuration
**ATTENTION: Modify the [system/config.scm](system/config.scm) file as per your device, especially `(file-systems)` variable. Wrong file system may cause data loss.**

To build the system image, run this:
```
guix system build system/system.scm
```

To reconfigure the system you are currently working on, run this as root:
```
guix system reconfigure system/system.scm
```

### Home configuration
You can modify the [home/config.scm](home/config.scm) as per your needs.

To build the home profile, run this:
```
guix home build home/home.scm
```

To reconfigure your current home profile, run this:
```
guix home reconfigure home/home.scm
```
