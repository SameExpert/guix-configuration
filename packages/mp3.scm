;;; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (mp3)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix svn-download)
  #:use-module (guix build-system cmake)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages mp3))

(define-public taglib-extras
  (package
    (name "taglib-extras")
    ;; 2 changes after mentioning version 1.0.2 in ChangeLog, which fixes
    ;; finding taglib.
    (version "1.0.2-2")
    (source
     (origin
       (method svn-fetch)
       (uri
        (svn-reference
          (url "svn://anonsvn.kde.org/home/kde/trunk/kdesupport/taglib-extras")
          (revision 1444333)))
       (file-name (string-append name "-" version "-checkout"))
       (sha256
        (base32
         "1qsima3ln6j63mzdp5y1yjaijxvk3wnriwld5ijdp1xv1dx4hqjc"))))
    (build-system cmake-build-system)
    (inputs
     (list taglib))
    (arguments
     (list #:tests? #f)) ;no test suite
    (home-page "https://websvn.kde.org/trunk/kdesupport/taglib-extras")
    (synopsis "Additional TagLib plugins")
    (description
     "This package provides TagLib plugins for Audible and RealMedia tags.")
    (license (list license:lgpl2.1+ license:gpl2+))))
