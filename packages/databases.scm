;;; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (databases)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (gnu packages databases))

(define-public mariadb-10.10.2
  (package/inherit mariadb
    (arguments
     (substitute-keyword-arguments (package-arguments mariadb)
       ((#:configure-flags flags)
        #~(append '("-DWITH_EMBEDDED_SERVER=ON") #$flags))))))
