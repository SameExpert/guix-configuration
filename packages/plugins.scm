;;; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (plugins)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system copy)
  #:use-module (guix gexp)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages xorg)
  #:use-module (nonguix build-system binary)
  #:use-module ((nonguix licenses) #:prefix license:))

(define-public apricot
  (package
    (name "apricot")
    (version "1.1.30")
    ;; Purchase "ApricotSynthLinux_v1.1.30.tar.gz" from
    ;; https://nakst.itch.io/apricot
    (source #f)
    (build-system binary-build-system)
    (arguments
     (list #:patchelf-plan
           #~`(("Apricot" ("libc" "libx11" "pulseaudio"))
               ("Apricot.clap" ("libc" "libx11")))
           #:install-plan
           #~`(("Apricot" "bin/")
               ("Apricot.clap" "lib/clap/")
               ("EULA.txt"
                ,#$(string-append "share/doc/apricot-" version "/")))
           #:phases
           #~(modify-phases %standard-phases
               (add-before 'patchelf 'dd-plug-dat
                 (lambda _
                   (substitute* "install.sh"
                     (("plug1\\.dat") "Apricot")
                     (("plug2\\.dat") "Apricot.clap")
                     (("plug3\\.dat") "Apricot.desktop")
                     (("\\+x Apricot\\.clap") "+x Apricot Apricot.clap")
                     (("whoami") "#whoami")
                     (("which") "#which")
                     (("^!") "#!")
                     (("ldd") "exit\nldd"))
                   (invoke "sh" "install.sh"))))))
    (inputs
     (list libx11 pulseaudio))
    (propagated-inputs
     (list gtk+ zenity))
    (supported-systems
     (list "x86_64-linux"))
    (home-page "https://nakst.gitlab.io/apricot/")
    (synopsis "Hybrid synthesizer")
    (description "Apricot is a hybrid synthesizer. It comes with a CLAP plugin
format and a standalone executable.")
    (license (license:nonfree "file:///ApricotLinux/EULA.txt"))))

(define-public fluctus
  (package
    (name "fluctus")
    (version "1.1.12")
    ;; Purchase "FluctustSynthLinux_v1.1.12.tar.gz" from
    ;; https://nakst.itch.io/fluctus
    (source #f)
    (build-system binary-build-system)
    (arguments
     (list #:patchelf-plan
           #~`(("Fluctus" ("libc" "libx11" "pulseaudio"))
               ("Fluctus.clap" ("libc" "libx11")))
           #:install-plan
           #~`(("Fluctus" "bin/")
               ("Fluctus.clap" "lib/clap/")
               ("EULA.txt"
                ,#$(string-append "share/doc/fluctus-" version "/")))
           #:phases
           #~(modify-phases %standard-phases
               (add-before 'patchelf 'dd-plug-dat
                 (lambda _
                   (substitute* "install.sh"
                     (("plug1\\.dat") "Fluctus")
                     (("plug2\\.dat") "Fluctus.clap")
                     (("plug3\\.dat") "Fluctus.desktop")
                     (("\\+x Fluctus\\.clap") "+x Fluctus Fluctus.clap")
                     (("whoami") "#whoami")
                     (("which") "#which")
                     (("^!") "#!")
                     (("ldd") "exit\nldd"))
                   (invoke "sh" "install.sh"))))))
    (inputs
     (list libx11 pulseaudio))
    (propagated-inputs
     (list gtk+ zenity))
    (supported-systems
     (list "x86_64-linux"))
    (home-page "https://nakst.gitlab.io/fluctus/")
    (synopsis "Hybrid synthesizer")
    (description "Fluctus is a hybrid synthesizer. It comes with a CLAP plugin
format and a standalone executable.")
    (license (license:nonfree "file:///FluctusLinux/EULA.txt"))))

(define-public regency
  (package
    (name "regency")
    (version "1.1.0")
    ;; Purchase "RegencySynthLinux_v1.1.0.tar.gz" from
    ;; https://nakst.itch.io/regency
    (source #f)
    (build-system binary-build-system)
    (arguments
     (list #:patchelf-plan
           #~`(("Regency" ("libc" "libx11" "pulseaudio"))
               ("Regency.clap" ("libc" "libx11")))
           #:install-plan
           #~`(("Regency" "bin/")
               ("Regency.clap" "lib/clap/")
               ("EULA.txt"
                ,#$(string-append "share/doc/regency-" version "/")))
           #:phases
           #~(modify-phases %standard-phases
               (add-before 'patchelf 'dd-plug-dat
                 (lambda _
                   (substitute* "install.sh"
                     (("plug1\\.dat") "Regency")
                     (("plug2\\.dat") "Regency.clap")
                     (("plug3\\.dat") "Regency.desktop")
                     (("\\+x Regency\\.clap") "+x Regency Regency.clap")
                     (("whoami") "#whoami")
                     (("which") "#which")
                     (("^!") "#!")
                     (("ldd") "exit\nldd"))
                   (invoke "sh" "install.sh"))))))
    (inputs
     (list libx11 pulseaudio))
    (propagated-inputs
     (list gtk+ zenity))
    (supported-systems
     (list "x86_64-linux"))
    (home-page "https://nakst.gitlab.io/regency/")
    (synopsis "Hybrid synthesizer")
    (description "Regency is a hybrid synthesizer. It comes with a CLAP plugin
format and a standalone executable.")
    (license (license:nonfree "file:///RegencyLinux/EULA.txt"))))
