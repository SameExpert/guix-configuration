;;; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (networking)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system qt)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages check)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages gnupg)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python-xyz)
  #:use-module (cpp))

(define-public garuda-downloader
  (let ((revision "0")
        (commit "8c0ab5d8e989420b32bcf1d30e79e6162c669730"))
    (package
      (name "garuda-downloader")
      (version (git-version "0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri
          (git-reference
            (url
             (string-append "https://gitlab.com/garuda-linux/applications"
                            "/garuda-downloader"))
            (commit commit)
            (recursive? #t))) ;for QDarkStyleSheet
         (file-name (git-file-name name version))
         (sha256
          (base32 "167jc3jg6p2l2dq3521hib809hvm1sv0nhyv2n2wa99vfnfdan0f"))
         (modules '((guix build utils)))
         (snippet
          ;; Unbundle zsync2
          '(begin
             (delete-file-recursively "zsync2")
             (substitute* "CMakeLists.txt"
               (("add_subdirectory\\(zsync2 EXCLUDE_FROM_ALL\\)")
                "find_package(zsync2 REQUIRED)"))))))
      (build-system qt-build-system)
      (arguments
       (list #:tests? #f)) ;no test rules
      (native-inputs
       (list pkg-config))
      (inputs
       (list libgcrypt python-qdarkstyle zsync2))
      (home-page
       (string-append "https://gitlab.com/garuda-linux/applications"
                      "/garuda-downloader"))
      (synopsis "Utility to download Garuda Linux bootable images")
      (description
       "This package provides a GUI interface to download Garuda Linux bootable
image files.")
      (license license:gpl3+))))

(define-public zsync2
  (package
    (name "zsync2")
    (version "2.0.0-alpha-1-20230304")
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
          (url "https://github.com/AppImageCommunity/zsync2")
          (commit version)))
       (sha256
        (base32 "07g39lrb703apngslhlj4x9nn9q1q3kwhkcx67wcsv96fh8qq9rq"))))
    (build-system cmake-build-system)
    (arguments
     (list #:configure-flags
           #~(list "-DCPR_FORCE_USE_SYSTEM_CURL=ON"
                   "-DUSE_SYSTEM_CPR=ON"
                   "-DUSE_SYSTEM_ARGS=ON")))
    (native-inputs
     (list args googletest pkg-config))
    (inputs
     (list cpr curl libgcrypt zlib))
    (home-page "https://github.com/AppImageCommunity/zsync2")
    (synopsis "File download/sync tool")
    (description
     "@command{zsync2} is a rewrite of @code{zsync-curl}, a file download/sync
tool, using modern C++, providing both a library and a standalone tool.")
    (license license:artistic2.0)))
