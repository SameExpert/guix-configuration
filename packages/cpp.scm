;;; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (cpp)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system meson)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages check)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages image)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages texinfo)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages webkit)
  #:use-module (gnu packages xorg))

(define-public args
  (package
    (name "args")
    (version "6.4.6")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/Taywee/args")
                    (commit version)))
              (sha256
               (base32
                "0bjpd5bkhkyw8p8njljkihs8lhaifvsgvva2anblz5q0fj3zf0fs"))))
    (build-system cmake-build-system)
    (home-page "https://github.com/Taywee/args")
    (synopsis "Header-only C++ argument parser library")
    (description
     "This is a header-only C++ argument parser library. It attempts to be
compatible with the functionality of the Python standard argparse library
(though not necessarily the API).")
    (license license:expat)))

(define-public bitsery
  (package
    (name "bitsery")
    (version "5.2.3")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/fraillt/bitsery")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1hv2fya7w53bfhlk79b1qnjg1qy076s8kvg22sfdq05bh0hxqrxf"))))
    (build-system cmake-build-system)
    (arguments
     (list #:configure-flags #~(list "-DBITSERY_BUILD_TESTS=ON")))
    (native-inputs (list googletest))
    (synopsis "Header only C++ binary serialization library")
    (description "This package provides header only C++ binary serialization
library.  It is designed around the networking requirements for real-time data
delivery, especially for games.")
    (home-page "https://github.com/fraillt/bitsery")
    (license license:expat)))

(define-public asyncplusplus
  (package
    (name "asyncplusplus")
    (version "1.2")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/Amanieu/asyncplusplus")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0iswbh7y46kn412c52af0n8bc4fplm3y94yh10n2lchispzar72j"))
              (modules '((guix build utils)))
              (snippet
               ;; Fix install location of cmake files.
               '(substitute* "CMakeLists.txt"
                  (("DESTINATION cmake")
                    "DESTINATION ${CMAKE_INSTALL_LIBDIR}/cmake")))))
    (build-system cmake-build-system)
    (arguments
     (list #:tests? #f)) ;no tests
    (home-page "https://github.com/Amanieu/asyncplusplus")
    (synopsis "Concurrency framework for C++11")
    (description "Async++ is a concurrency framework for C++11.")
    (license license:expat)))

(define-public cpr
  (package
    (name "cpr")
    (version "1.10.5")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/libcpr/cpr")
                    (commit version)))
              (sha256
               (base32
                "1v4cnfz36z7zs63gx433km9fjbq65c1bda19pj3ycxvww7d982wq"))))
    (build-system cmake-build-system)
    (arguments
     (list #:tests? #f ;requires fetching mongoose while building
           #:configure-flags
           #~(list "-DCPR_USE_SYSTEM_CURL=ON")))
    (inputs
     (list curl openssl))
    (home-page "https://docs.libcpr.org/")
    (synopsis "Wrapper around libcurl")
    (description
     "C++ Requests is a wrapper around libcurl.")
    (license license:expat)))

(define-public function2
  (package
    (name "function2")
    (version "4.2.4")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/Naios/function2")
                    (commit version)))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0l4kn81lfi687mv2d2mkrrqgjp9v186jlfshrgcr5717lwqw39xg"))
              (modules '((guix build utils)))
              (snippet
               ;; Unbundle googletest.
               '(begin
                  (delete-file-recursively "test")
                  (substitute* "CMakeLists.txt"
                    (("add_subdirectory\\(test\\)") ""))))))
    (build-system cmake-build-system)
    ;; The test size_match_layout fails on i586/i686. For more info:
    ;; https://github.com/Naios/function2/issues/57
    (arguments
     (list #:tests? #f))
    (synopsis "Improved implementations of std::function")
    (description "This package provides the following implementations of
std::function:
@itemize
@item copyable fu2::function
@item move-only fu2::unique_function (capable of holding move only types)
@item non-owning fu2::function_view (capable of referencing callables in a non
owning way)
@end itemize")
    (home-page "https://naios.github.io/function2/")
    (license license:boost1.0)))

(define-public libbinio
  (package
    (name "libbinio")
    (version "1.5")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/adplug/libbinio")
                    (commit (string-append "libbinio-" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "010q3dgn8zdv5hnvbhhr0jzgzvhrmil395hx585yy7gsi8dsyr35"))
              (modules '((guix build utils)))
              (snippet
               `(substitute* "doc/libbinio.texi"
                  ;; Do not include version.texi which does not exist yet.
                  ((".*version\\.texi.*") "")
                  ;; Replace VERSION with the package version.
                  (("@value\\{VERSION\\}") ,version)
                  ;; Replace UPDATED with the date of the current commit.
                  (("@value\\{UPDATED\\}") "7 August 2019")))))
    (native-inputs
     (list autoconf automake libtool texinfo))
    (build-system gnu-build-system)
    (home-page "http://adplug.github.io/libbinio/")
    (synopsis "Binary I/O stream class library")
    (description
     "This binary I/O stream class library presents a platform-independent way
to access binary data streams in C++.  The library is hardware independent in
the form that it transparently converts between the different forms of
machine-internal binary data representation.")
    (license license:lgpl2.1+)))

(define-public juce
  (package
    (name "juce")
    (version "7.0.5")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/juce-framework/JUCE")
                    (commit version)))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1zfkyf9gwwvv0mrslvvjlh6gxv1zd50bkcal54x1kqil46hps49m"))))
    (build-system cmake-build-system)
    (arguments
     (list #:tests? #f ;no test suite
           #:configure-flags #~(list "-DJUCE_TOOL_INSTALL_DIR=bin")
           #:phases
           #~(modify-phases %standard-phases
               (add-after 'unpack 'patch-paths
                 (lambda* (#:key inputs #:allow-other-keys)
                   (substitute*
                    (find-files "." "jucer_ProjectExport_CodeBlocks.h$")
                     (("/usr/include/freetype2")
                      (search-input-directory inputs "/include/freetype2")))
                   (substitute*
                    (find-files "." "juce_linux_Fonts.cpp$")
                     (("fonts\\.conf\" };")
                      (string-append
                        "fonts.conf\"\n\""
                        (search-input-file inputs "/etc/fonts/fonts.conf")
                        "\"\n};"))))))))
    (native-inputs
     (list alsa-lib
           curl
           jack-1
           libx11
           pkg-config
           webkitgtk-with-libsoup2))
    (inputs (list fontconfig freetype libjpeg-turbo libpng))
    (home-page "https://juce.com")
    (synopsis "C++ application framework for audio plugins and plugin hosts")
    (description
     "JUCE is a C++ application framework for creating applications including
VST, VST3, AU, AUv3, AAX and LV2 audio plug-ins and plug-in hosts.")
    (license
     (list license:asl2.0 ;for Oboe and AudioUnitSDK
           license:bsd-3 ;for FLAC, Ogg Vorbis and OpenGL Extension Wrangler
           license:expat ;for Mesa 3-D graphics and jucer icons
           license:gpl3 ;for JUCE and VST3 SDK
           license:ijg ;for jpeglib
           license:isc ;for LV2 SDK
           license:zlib)))) ;for pngLib, zlib and Box2D
