;;; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (kde)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system qt)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix gexp)
  #:use-module (guix utils)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages base)
  #:use-module (gnu packages cryptsetup)
  #:use-module (gnu packages kde)
  #:use-module (gnu packages kde-frameworks)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages polkit)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages xdisorg))

(define-public kcron
  (package
    (name "kcron")
    (version "24.12.1")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "mirror://kde/stable/release-service/" version
                           "/src/kcron-" version ".tar.xz"))
       (sha256
        (base32 "1r9nr4qds82gjvz0n2c7bxxx4h0zlpxhkcwd46q2d23ppnjj1gcd"))))
    (build-system qt-build-system)
    (arguments
     (list #:qtbase qtbase))
    (native-inputs
     (list extra-cmake-modules kdoctools))
    (inputs
     (list kauth
           kcmutils
           kconfigwidgets
           kcoreaddons
           kdeclarative
           ki18n
           kio
           kirigami
           kirigami-addons))
    (home-page "https://invent.kde.org/system/kcron")
    (synopsis "Task scheduler")
    (description "KCron is a task scheduler by KDE.")
    (license license:gpl2+)))

(define-public kpmcore-24.12.1
  (package/inherit kpmcore
    (arguments
     (substitute-keyword-arguments (package-arguments kpmcore)
       ((#:phases phases)
        #~(modify-phases #$phases
            (replace 'fix-cmake-install-directories
              (lambda _
                (substitute* "src/util/CMakeLists.txt"
                  (("DESTINATION \\$\\{POLKITQT-1_POLICY_FILES_INSTALL_DIR\\}")
                   "DESTINATION share/polkit-1/actions"))
                (substitute* "src/backend/corebackend.cpp"
                  (("\\/usr") #$output))))
            (add-before 'configure 'patch-trustedprefixes-file
              (lambda* (#:key inputs #:allow-other-keys)
                (call-with-output-file "src/util/trustedprefixes"
                  (lambda (port)
                    (map (lambda (prefix)
                           (display prefix port)
                           (newline port))
                         (list (assoc-ref inputs "coreutils")
                               (assoc-ref inputs "util-linux")
                               (assoc-ref inputs "eudev")
                               (assoc-ref inputs "cryptsetup")
                               (assoc-ref inputs "lvm2")
                               (assoc-ref inputs "mdadm")
                               (assoc-ref inputs "smartmontools")
                               "/run/current-system/profile"
                               "/usr"
                               "/"))))))))))
    (inputs
     `(("coreutils" ,coreutils)
       ("cryptsetup" ,cryptsetup)
       ("eudev" ,eudev)
       ("kauth" ,kauth)
       ("kcoreaddons" ,kcoreaddons)
       ("ki18n" ,ki18n)
       ("kwidgetsaddons" ,kwidgetsaddons)
       ("lvm2" ,lvm2)
       ("mdadm" ,mdadm)
       ("polkit-qt6" ,polkit-qt6)
       ("qtbase" ,qtbase)
       ("qca-qt6" ,qca-qt6)
       ("smartmontools" ,smartmontools)
       ("util-linux" ,util-linux)
       ("util-linux:lib" ,util-linux "lib")))))

(define-public melon
  (let ((commit "9b09bbda83f09d64e75f2c878cbc5178d79a0426")
        (revision "0"))
    (package
      (name "melon")
      (version (git-version "0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://invent.kde.org/system/melon")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "1vnyvp2yblli4llpsayqj657khxc48bp9kp45n9kiyqlfh27jq95"))))
      (build-system qt-build-system)
      (arguments
       (list #:qtbase qtbase))
      (native-inputs
       (list extra-cmake-modules))
      (inputs
       (list baloo
             kbookmarks
             kconfig
             ki18n
             kio
             kitemmodels
             kwindowsystem
             kxmlgui
             libxkbcommon
             nongurigaeru))
      (home-page "https://invent.kde.org/system/melon")
      (synopsis "Desktop QML file manager")
      (description "Melon is a desktop QML file manager.")
      (license license:gpl3+))))

(define-public nongurigaeru
  (let ((commit "0d2a1cc9bdaa4bb309416d1b65433c100d7866ff")
        (revision "0"))
    (package
      (name "nongurigaeru")
      (version (git-version "0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://invent.kde.org/libraries/nongurigaeru")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "18pa2bq5h1h7asn3i9b40hi3x5gyp33vrjjn759zc51rfmp8jqc9"))))
      (build-system qt-build-system)
      (arguments
       (list #:qtbase qtbase
             #:configure-flags
             #~(list "-DBUILD_TESTING=ON")
             #:phases
             #~(modify-phases %standard-phases
                 ;; Set path to find TestNGLib binary.
                 (add-before 'check 'set-path
                   (lambda _
                     (setenv "PATH"
                             (string-append (getcwd) "/bin" ":"
                                            (getenv "PATH"))))))))
      (native-inputs
       (list extra-cmake-modules))
      (inputs
       (list kconfig
             kcoreaddons
             kdbusaddons
             ki18n))
      (home-page "https://invent.kde.org/libraries/nongurigaeru")
      (synopsis "Missing Foundation library for Qt GUI apps")
      (description "Nongurigaeru is the missing Foundation library for Qt GUI
apps.

Its features include:
@itemize
@item State restoration abstractions
@item Drag & drop components
@item User-editable toolbars
@end itemize")
      (license license:gpl3+))))

(define-public partitionmanager
  (package
    (name "partitionmanager")
    (version "24.12.1")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "mirror://kde/stable/release-service/" version
                           "/src/partitionmanager-" version ".tar.xz"))
       (sha256
        (base32 "1c535iaziblv0d47nzm298q7k8z04kmzspi1nzr5awxgavd033ws"))))
    (build-system qt-build-system)
    (arguments
     (list #:qtbase qtbase))
    (native-inputs
     (list extra-cmake-modules kdoctools))
    (inputs
     (list kconfig
           kconfigwidgets
           kcoreaddons
           kcrash
           kdbusaddons
           ki18n
           kio
           kjobwidgets
           kpmcore-24.12.1
           kwidgetsaddons
           kwindowsystem
           kxmlgui
           polkit-qt6))
    (home-page "https://apps.kde.org/partitionmanager/")
    (synopsis "Disk device, partition and file system manager")
    (description "KDE Partition Manager is a utility to help you manage the
disks, partitions, and file systems.  It allows you to easily create, copy,
move, delete, back up, restore, and resize them without losing data.  It
supports a large number of file systems, including ext2/3/4, btrfs, NTFS,
FAT16/32, JFS, XFS and more.")
    (license license:gpl3+)))
