;;; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (audio)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix utils)
  #:use-module (guix git-download)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system cargo)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system meson)
  #:use-module (guix build-system qt)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages backup)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages cpp)
  #:use-module (gnu packages crates-io)
  #:use-module (gnu packages documentation)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages networking)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages ruby)
  #:use-module (gnu packages sdl)
  #:use-module (gnu packages texinfo)
  #:use-module (gnu packages wine)
  #:use-module (gnu packages xorg)
  #:use-module (ice-9 match)
  #:use-module (cpp)
  #:use-module (crates-io)
  #:use-module (qt))

(define-public adplug
  (package
    (name "adplug")
    (version "2.3.3")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/adplug/adplug")
                    (commit (string-append "adplug-" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1r23fndz8lpyga398qxp4iy8hxabmq5kx5n9407bx47sc32j3lnn"))
              (modules '((guix build utils)))
              (snippet
               ;; Include mididata.h for installation.
               '(substitute* "Makefile.am"
                  (("pkginclude_HEADERS = ")
                   (string-append "pkginclude_HEADERS = src/mididata.h "))))))
    (native-inputs
     (list autoconf automake libbinio libtool pkg-config texinfo))
    (build-system gnu-build-system)
    (home-page "https://adplug.github.io/")
    (synopsis "AdLib sound player library")
    (description
     "AdPlug is a hardware independent AdLib sound player library, mainly
written in C++.  AdPlug plays sound data, originally created for the AdLib
(OPL2/3) audio board, on top of an OPL2/3 emulator or by using the real
hardware.  No OPL2/3 chips are required for playback.")
    (license license:lgpl2.1+)))

(define-public ringbuffer
  (package
    (name "ringbuffer")
    (version "0.9.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/JohannesLorenz/ringbuffer")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1c57hj6zyvcjpcbwrq8c1hj5brk1bnh69ayd88ny1p0gx031sjpk"))))
    (build-system cmake-build-system)
    (arguments
     (list #:configure-flags
           #~(list (string-append "-DINSTALL_LIB_DIR=" #$output "/lib"))
           #:phases
           #~(modify-phases %standard-phases
               (add-after 'install 'install-export-header
                 (lambda _
                   (install-file "src/lib/ringbuffer_export.h"
                                 (string-append #$output
                                                "/include/ringbuffer")))))))
    (home-page "https://github.com/JohannesLorenz/ringbuffer")
    (synopsis "Lock-free multi-reader ringbuffer")
    (description
     "@code{ringbuffer} is a library containing a ringbuffer.  It is lock-free
(using atomics only), and allows multiple readers, but only one writer.")
    (license license:gpl3+)))

(define-public clap
  (package
    (name "clap")
    (version "1.1.10")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/free-audio/clap")
                    (commit version)))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0skn3cvh7zs173v3i6ywdmddqzrhxvivwdisvmqc6hvq594f8z80"))))
    (build-system cmake-build-system)
    (synopsis "Audio Plugin API")
    (description
     "CLAP stands for CLever Audio Plugin.  It is an audio plugin ABI which
defines a standard for Digital Audio Workstations and audio plugins to work
together.")
    (home-page "https://cleveraudio.org/")
    (license license:expat)))

(define-public clap-1.1.9
  (let ((version "1.1.9"))
    (package/inherit clap
      (version version)
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://github.com/free-audio/clap")
                      (commit version)))
                (file-name (git-file-name "clap" version))
                (sha256
                 (base32
                  "0z1van2wj68qv5rcvf19rc4xg57ywycdzmc0wrzss334cd9z8qyg")))))))

(define-public carla-2.6.0-17000e7
  (let ((commit "17000e7fe99459b25a50094a8b00bdfa12f2bfbc")
        (revision "1"))
    (package/inherit carla-2.6
      (version (git-version "2.6.0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri
           (git-reference
             (url "https://github.com/falkTX/Carla")
             (commit commit)))
         (file-name (git-file-name (package-name carla-2.6) version))
         (sha256
          (base32
           "1bils2xr4zv4z70al0rg6bc1ijvn2cq6macq11skx7gzjlcw09bq"))))
      (arguments
       (substitute-keyword-arguments (package-arguments carla-2.6)
         ((#:phases phases)
          #~(modify-phases #$phases
              (replace 'make-carla-executable
                (lambda _
                  (with-directory-excursion (string-append #$output
                                                           "/share/carla")
                    (for-each (lambda (file)
                              (chmod file #o555))
                     (list "carla"
                           "carla-control"
                           "carla-jack-multi"
                           "carla-jack-single"
                           "carla-patchbay"
                           "carla-rack")))))
              (replace 'wrap-executables
                (lambda* (#:key inputs #:allow-other-keys)
                  (with-directory-excursion #$output
                    (for-each (lambda (file)
                                ;; Wrap only those executable files that other
                                ;; programs (e.g. lmms) would call.
                                (when (and (executable-file? file)
                                           (not (symbolic-link? file))
                                           (not (string-suffix? ".py" file)))
                                  (wrap-script file
                                    #:guile (search-input-file inputs
                                                               "bin/guile")
                                    `("GUIX_PYTHONPATH" ":" prefix
                                      (,(getenv "GUIX_PYTHONPATH")))
                                    `("QT_PLUGIN_PATH" ":" prefix
                                      (,(getenv "QT_PLUGIN_PATH"))))))
                   (append (find-files "bin")
                           (find-files "share/carla/resources"))))))))))
      (inputs
       (modify-inputs (package-inputs carla-2.6)
         (replace "python-pyqt" python-pyqt-6)
         (replace "qtbase" qtbase)
         (append pulseaudio qtwayland sdl2))))))

(define-public carla-2.6.0-17000e7-qt5
  (package/inherit carla-2.6.0-17000e7
    (name "carla-qt5")
    (inputs
      (modify-inputs (package-inputs carla-2.6.0-17000e7)
        (replace "python-pyqt" python-pyqt)
        (replace "qtbase" qtbase-5)
        (replace "qtwayland" qtwayland-5)))))

(define-public jamesdsp
  (package
    (name "jamesdsp")
    (version "2.7.0")
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
          (url "https://github.com/Audio4Linux/JDSP4Linux")
          (commit version)
          ;; Recurse GraqhicEQWidget, FlatTabWidget, LiquidEqualizerWidget and
          ;; EELEditor.
          (recursive? #t)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "17vx12kbvwxvb69vzrlb82mrgf6sl3plyk71g9f39p49ialdsnbr"))
       (modules '((guix build utils)))
       (snippet
        ;; Unbundle 3rd party libraries.
        ;; NOTE: QCodeEditor from system fails to build EELEditor. WAF from
        ;; system fails to build FlatTabWidget. 3rd party library found in
        ;; LiquidEqualizerWidget is unknown. Hence these libraries found in
        ;; their respective directories are untouched.
        '(begin
           ;; Delete the bundled 3rd party libraries.
           (for-each delete-file-recursively
            (list "3rdparty"
                  "src/subprojects/EELEditor/3rdparty/docking-system"
                  "src/subprojects/EELEditor/src/EELEditor-Linker.pri"))
           (with-directory-excursion "src"
             (substitute* "src.pro"
               ;; Do not use bundled 3rd party libraries.
               ((".*3rdparty.*") "")
               ;; Link required libraries from system.
               (("-ldl")
                (string-join '("-ldl"
                               "-lasync++"
                               "-lqcustomplot"
                               "-lqt6advanceddocking"
                               "-lqtcsv"
                               "-lwaf")
                               " ")))
             ;; Fix including WAF headers.
             (substitute* "MainWindow.cpp"
                       (("<Animation") "<WAF/Animation"))
             ;; Do not use resources from the bundled docking-system.
             (substitute* '("interface/fragment/AppManagerFragment.ui")
               ((".*location.*3rdparty.*") "")
               ((" resource=.*>") ">"))
             (with-directory-excursion "subprojects/EELEditor/src"
               ;; Do not use bundled docking-system.
               (substitute* "EELEditor.pri"
                 ((".*docking-system.*") ""))
               ;; Do not link to bundled docking-system.
               (substitute* "src.pro"
                 ((".*EELEditor-Linker.*") ""))
               ;; Fix including headers from the system.
               (substitute* '("eeleditor.cpp"
                              "eeleditor.h")
                 (("<Dock") "<qt6advanceddocking/Dock")
                 (("<FloatingDock") "<qt6advanceddocking/FloatingDock"))))))))
    (build-system qt-build-system)
    (arguments
     (list #:qtbase qtbase
           #:tests? #f ;no tests
           #:phases
           #~(modify-phases %standard-phases
               (replace 'configure
                 (lambda* (#:key inputs #:allow-other-keys)
                   (invoke "qmake" (string-append "PREFIX=" #$output))))
               (add-after 'install 'install-icon
                 (lambda _
                   (let ((pixmaps (string-append #$output "/share/pixmaps")))
                     (mkdir-p pixmaps)
                     (copy-file "resources/icons/icon.png"
                                (string-append pixmaps "/jamesdsp.png")))))
               (add-after 'install-icon 'create-desktop-entry-file
                 (lambda _
                   (make-desktop-entry-file
                    (string-append #$output
                                  "/share/applications/jamesdsp.desktop")
                    #:name "JamesDSP"
                    #:comment "Audio effect processor"
                    #:keywords '("equalizer" "audio" "effect")
                    #:categories '("AudioVideo" "Audio")
                    #:exec (string-append #$output "/bin/jamesdsp")
                    #:icon (string-append #$output "/share/pixmaps/jamesdsp.png")
                    #:startup-notify #f))))))
    (native-inputs
     (list pkg-config))
    (inputs
     (list asyncplusplus
           glibmm-2.66
           libarchive
           pipewire
           qcustomplot
           qt-advanced-docking-system
           qtcsv
           qtpromise
           qtsvg
           qt-widget-animation-framework))
    (home-page "https://github.com/Audio4Linux/JDSP4Linux")
    (synopsis "Audio effect processor for PipeWire and PulseAudio clients")
    (description "JamesDSP is an audio effect processor for PipeWire and
PulseAudio clients, featuring:
@itemize
@item Automatic bass boost: Frequency-detecting bass-boost
@item Automatic dynamic range compressor: automated multiband dynamic range
 adjusting effect
@item Complex reverberation IIR network (Progenitor 2)
@item Interpolated FIR equalizer with flexible bands
@item Arbitrary response equalizer (also known as GraphicEQ from EqualizerAPO)
@item AutoEQ database integration (requires network connection)
@item Partitioned convolver (Auto segmenting convolution): Mono, stereo,
 full/true stereo (LL, LR, RL, RR) impulse response
@item Crossfeed: Realistic surround effects
@item Soundstage wideness: A multiband stereo wideness controller
@item ViPER-DDC: Parametric equalization on audio and creating VDC input files
@item Analog modeling: An aliasing-free even harmonic generator
@item Output limiter
@item Scripting engine: Live programmable DSP using the EEL2 scripting language
@item Scripting IDE featuring syntax highlighting, basic code completion,
 dynamic code outline window, console output support and detailed error
 messages with inline code highlighting
@end itemize")
    (license (list license:gpl3+
                   license:gpl2      ;LiquidEqualizerWidget's 3rd party library
                   license:gpl2+     ;GraphicEQWidget
                   license:expat)))) ;QAnimatedSlider and QCodeEditor

(define-public rtosc
  (package
    (name "rtosc")
    (version "0.3.1")
    (source (origin
             (method git-fetch)
             (uri (git-reference
                   (url "https://github.com/fundamental/rtosc")
                   (commit (string-append "v" version))))
             (file-name (git-file-name name version))
             (sha256
              (base32
               "1djvyq53cjwd0szkvhpk45zcmdgrlirjwr02nqq9hzdmh0n26pk2"))
             (patches
              (list
                (local-file
                 "patches/rtosc-0.3.1-fix-invalid-comparison-operator.patch")))))
    (build-system cmake-build-system)
    (arguments
     (list #:configure-flags
           #~(list "-DPERF_TEST=ON"
                   "-DRTOSC_BUILD_SHARED_LIBS=ON")
           #:phases
           #~(modify-phases %standard-phases
               (add-after 'build 'build-documentation
                 (lambda _
                   (invoke "make" "rtosc-doc")))
               (add-after 'install 'install-documentation
                 (lambda _
                     (copy-recursively "html"
                      (string-append #$output:doc
                       "/share/doc/rtosc/html")))))))
    (native-inputs
     (list doxygen pkg-config ruby))
    (inputs
     (list jack-1
           liblo
           libx11
           mesa))
    (outputs (list "out" "doc"))
    (home-page "https://fundamental-code.com/wiki/rtosc/")
    (synopsis "Realtime Safe OSC packet serialization and dispatch")
    (description
     "RtOsc is a realtime safe library for handling OSC messages.")
    (license license:expat)))

(define-public yabridge
  (package
    (name "yabridge")
    (version "5.1.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/robbert-vdh/yabridge")
                    (commit version)))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1wypz1hnnizfxnk2j1rl740nf8qna0nymdi9kxy2p2k9wwcrsx5y"))
              (patches
               (list
                (local-file "patches/yabridge-5.0.4-dependencies.patch")))))
    (build-system meson-build-system)
    (arguments
     (list #:configure-flags
           #~(list "-Dsystem-asio=true"
                    (string-append "--cross-file=" #$source "/cross-wine.conf"))
           #:phases
           #~(modify-phases %standard-phases
               (add-after 'unpack 'add-vst3-subproject
                 (lambda* (#:key inputs #:allow-other-keys)
                   (symlink (assoc-ref inputs "vst3sdk") "subprojects/vst3")))
               (add-after 'unpack 'patch-paths
                 (lambda* (#:key inputs #:allow-other-keys)
                   (substitute* "src/chainloader/utils.cpp"
                     (("\"/usr/local/lib64\",")
                      (string-append "\"/usr/local/lib64\",\n\""
                                     #$output "/lib\",")))))
               (replace 'install
                 (lambda _
                   (for-each
                     (lambda (file)
                       (install-file file (string-append #$output "/bin")))
                       (find-files "." "-host\\.exe(|\\.so)$"))
                   (for-each
                     (lambda (file)
                       (install-file file (string-append #$output "/lib")))
                       (find-files "." "libyabridge")))))))
    (native-inputs
     ;; NOTE: Use the latest clap version with the next update of yabrigde.
     `(("clap" ,clap-1.1.9)
       ("cmake-minimal" ,cmake-minimal)
       ("function2" ,function2)
       ("gulrak-filesystem" ,gulrak-filesystem)
       ("pkg-config" ,pkg-config)
       ("tomlplusplus" ,tomlplusplus)
       ;; This is VST3 SDK v3.7.7_build_19 with the documentation and VSTGUI
       ;; submodules removed and a dummy `meson.build` file that just lists all
       ;; source files.
       ("vst3sdk"
        ,(origin
           (method git-fetch)
           (uri (git-reference
                 (url "https://github.com/robbert-vdh/vst3sdk")
                 (commit (string-append "v3.7.7_build_19-patched"))
                 ;; Required for vst3_base, vst3_pluginterfaces,
                 ;; and vst3_public_sdk.
                 (recursive? #t)))
           (file-name (git-file-name name version))
           (sha256
            (base32
             "09axvpshwbf5061kcbl26v74dcmwxmgmlxb15b75bnqbh0zcghrf"))
           (patches
            (list
             (local-file
              "patches/vst3sdk-3.7.7-allow-winelib-compilation.patch")))))))
    (inputs
     (list asio
           bitsery
           dbus
           libxcb
           wine64))
    (supported-systems
     (package-supported-systems wine64))
    (home-page "https://github.com/robbert-vdh/yabridge")
    (synopsis "Implementation of Windows VST2, VST3 and CLAP plugin APIs")
    (description
     "@code{yabridge} is Yet Another way to use Windows audio plugins.  It
supports using Windows VST2, VST3, and CLAP plugins in plugin hosts as if they
were native plugins, with optional support for plugin groups to enable
inter-plugin communication for VST2 plugins and quick startup times.")
    (license license:gpl3+)))

(define-public yabridgectl
  (package/inherit yabridge
    (name "yabridgectl")
    (build-system cargo-build-system)
    (arguments
     (list #:cargo-inputs
           `(("rust-anyhow" ,rust-anyhow-1)
             ("rust-clap" ,rust-clap-4)
             ("rust-colored" ,rust-colored-2)
             ("rust-is-executable" ,rust-is-executable-1)
             ("rust-goblin" ,rust-goblin-0.6)
             ("rust-libloading" ,rust-libloading-0.7)
             ("rust-promptly" ,rust-promptly-0.3)
             ("rust-rayon" ,rust-rayon-1)
             ("rust-reflink" ,rust-reflink-0.1)
             ("rust-serde" ,rust-serde-1)
             ("rust-serde-derive" ,rust-serde-derive-1)
             ("rust-serde-jsonrc" ,rust-serde-jsonrc-0.1)
             ("rust-textwrap" ,rust-textwrap-0.16)
             ("rust-toml" ,rust-toml-0.5)
             ("rust-walkdir" ,rust-walkdir-2)
             ("rust-which" ,rust-which-4)
             ("rust-xdg" ,rust-xdg-2))
           #:phases
           #~(modify-phases %standard-phases
               (add-after 'unpack 'change-directory
                (lambda _
                  (chdir "tools/yabridgectl")))
               (add-after 'change-directory 'patch-paths
                 (lambda* (#:key inputs #:allow-other-keys)
                   (with-directory-excursion "src"
                     (substitute* '("config.rs" "main.rs")
                       (("/usr") (assoc-ref inputs "yabridge"))))))
               (add-before 'configure 'patch-rust-reflink-version
                 (lambda _
                   (substitute* "Cargo.toml"
                     (("reflink = .*$")
                      (string-append "reflink = \""
                                     #$(package-version rust-reflink-0.1)
                                     "\"\n")))))
               (add-after 'install 'wrap-program
                (lambda* (#:key inputs #:allow-other-keys)
                  (wrap-program (string-append #$output "/bin/yabridgectl")
                    `("LD_LIBRARY_PATH" ":" prefix
                      (,(string-append (assoc-ref inputs "yabridge") "/lib")
                       ,(string-append (assoc-ref inputs "dbus") "/lib")))
                    `("PATH" ":" prefix
                      (,(string-append (assoc-ref inputs "yabridge")
                                                         "/bin")))))))))
    (native-inputs '())
    (inputs
     (list dbus))
    (propagated-inputs
     (list yabridge))
    (synopsis "Utility to set up and update yabridge")
    (description
     "@command{yabridgectl} is a tool to setup and update @code{yabridge}.")))
