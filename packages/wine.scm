;;; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (wine)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (nongnu packages wine))

(define-public winetricks-20240105
  (package/inherit winetricks
    (name "winetricks")
    (version "20240105")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/Winetricks/winetricks")
                    (commit version)))
                    (file-name (git-file-name name version))
              (sha256
               (base32
                "0v77abxabwal1qnl406nb76s3056xhx24gxg51wcx82sbxpj0cb1"))))))
