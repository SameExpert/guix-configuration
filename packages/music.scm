;;; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (music)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix utils)
  #:use-module ((ice-9 match))
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system qt)
  #:use-module ((nonguix build-system binary))
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module ((nonguix licenses) #:prefix license:)
  #:use-module (gnu packages algebra)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages cpp)
  #:use-module (gnu packages fcitx5)
  #:use-module (gnu packages fltk)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages java)
  #:use-module (gnu packages libusb)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages llvm)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages mp3)
  #:use-module (gnu packages music)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages ruby)
  #:use-module (gnu packages sdl)
  #:use-module (gnu packages tcl)
  #:use-module (gnu packages video)
  #:use-module (gnu packages wine)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages xorg)
  #:use-module (audio)
  #:use-module (mruby-xyz)
  #:use-module (qt))

(define-public reaper
  (package
    (name "reaper")
    (version "7.02")
    (source
      (origin
        (method url-fetch)
        (uri (string-append "https://www.reaper.fm/files/"
                            (version-major version)
                            ".x/reaper"
                            (string-replace-substring version "." "")
                            "_"
                            (match (%current-system)
                              ("x86_64-linux" "linux_x86_64")
                              ("i686-linux" "linux_i686")
                              ("aarch64-linux" "linux_aarch64")
                              ("armhf-linux" "linux_armv7l"))
                            ".tar.xz"))
        (sha256
         (base32
          (match (%current-system)
            ("x86_64-linux"
             "0apnkv28x2vdlh7q26xsrn3b0y06cj4xgwsl1dh4rkkylrcld87k")
            ("i686-linux"
             "00b4hdk0i4gqlk3h5y9s07fhpvklazl2f2mzbr34m41s7avfnmx5")
            ("aarch64-linux"
             "15skc16w3yl0fbs15h39xzf2xk1y5q1919fqrvwinq5r73ig55wh")
            ("armhf-linux"
             "1laaiw4dcmaxzf7g3f0dmlihk53p50xnk8rmj8nw18yf7qhcscbs"))))))
    (build-system binary-build-system)
    (arguments
     (list #:strip-binaries? #f ;allocated section `.dynsym' not in segment
           #:patchelf-plan
           #~(let ((libs (list "libc" "gcc")))
               (list (list "REAPER/reaper"
                           (append libs
                                   (list "alsa-lib" "jack" "pulseaudio")))
                     (list "REAPER/Plugins/reaper_host_x86_64"
                           (append libs
                                   (list "alsa-lib" "jack" "pulseaudio")))
                     (list "REAPER/Plugins/elastique3.so" libs)
                     (list "REAPER/Plugins/jsfx.so" libs)
                     (list "REAPER/Plugins/reaper_cd.so" libs)
                     (list "REAPER/Plugins/reaper_csurf.so" libs)
                     (list "REAPER/Plugins/reaper_ddp.so" libs)
                     (list "REAPER/Plugins/reaper_explorer.so" libs)
                     (list "REAPER/Plugins/reaper_flac.so" libs)
                     (list "REAPER/Plugins/reaper_midi.so" libs)
                     (list "REAPER/Plugins/reaper_mp3dec.so" libs)
                     (list "REAPER/Plugins/reaper_ogg.so" libs)
                     (list "REAPER/Plugins/reaper_opus.so" libs)
                     (list "REAPER/Plugins/reaper_video.so" libs)
                     (list "REAPER/Plugins/reaper_wave.so" libs)
                     (list "REAPER/Plugins/reaper_wavpack.so" libs)
                     (list "REAPER/Plugins/rubberband.so" libs)
                     (list "REAPER/Plugins/soundtouch.so" libs)
                     (list "REAPER/Plugins/FX/reacast.vst.so" libs)
                     (list "REAPER/Plugins/FX/reacomp.vst.so" libs)
                     (list "REAPER/Plugins/FX/reacontrolmidi.vst.so" libs)
                     (list "REAPER/Plugins/FX/readelay.vst.so" libs)
                     (list "REAPER/Plugins/FX/reaeq.vst.so" libs)
                     (list "REAPER/Plugins/FX/reafir.vst.so" libs)
                     (list "REAPER/Plugins/FX/reagate.vst.so" libs)
                     (list "REAPER/Plugins/FX/reainsert.vst.so" libs)
                     (list "REAPER/Plugins/FX/realimit.vst.so" libs)
                     (list "REAPER/Plugins/FX/reaninjam.vst.so" libs)
                     (list "REAPER/Plugins/FX/reapitch.vst.so" libs)
                     (list "REAPER/Plugins/FX/reasamplomatic.vst.so" libs)
                     (list "REAPER/Plugins/FX/reastream.vst.so" libs)
                     (list "REAPER/Plugins/FX/reasurround.vst.so" libs)
                     (list "REAPER/Plugins/FX/reasurround2.vst.so" libs)
                     (list "REAPER/Plugins/FX/reasyndr.vst.so" libs)
                     (list "REAPER/Plugins/FX/reacast.vst.so" libs)
                     (list "REAPER/Plugins/FX/reasynth.vst.so" libs)
                     (list "REAPER/Plugins/FX/reatune.vst.so" libs)
                     (list "REAPER/Plugins/FX/reaverb.vst.so" libs)
                     (list "REAPER/Plugins/FX/reaverbate.vst.so" libs)
                     (list "REAPER/Plugins/FX/reavocode.vst.so" libs)
                     (list "REAPER/Plugins/FX/reavoice.vst.so" libs)
                     (list "REAPER/Plugins/FX/reaxcomp.vst.so" libs)))
           #:phases
           #~(modify-phases %standard-phases
               (replace 'install
                 (lambda* (#:key outputs inputs #:allow-other-keys)
                   (let* ((target (string-append #$output "/opt"))
                          (bin (string-append #$output "/bin"))
                          (data (string-append #$output "/share"))
                          (doc (string-append data "/doc/reaper-" #$version)))
                              (setenv "HOME" "/tmp")
                     (setenv "XDG_DATA_HOME" data)
                     (invoke "sh" "./install-reaper.sh" "--install"
                             target "--integrate-user-desktop")
                     (delete-file (string-append target
                                 "/REAPER/uninstall-reaper.sh"))
                     (delete-file (string-append target
                                 "/REAPER/libSwell.so"))
                     (symlink (search-input-file inputs "/lib/libSwell.so")
                              (string-append target "/REAPER/libSwell.so"))
                     (mkdir-p bin)
                     (symlink (string-append target "/REAPER/reaper")
                              (string-append bin "/reaper"))
                     (mkdir-p doc)
                     (symlink (string-append target "/REAPER/EULA.txt")
                              (string-append doc "/LICENSE"))))))))
    (native-inputs
      (list xdg-utils))
    (inputs
      (list alsa-lib `(,gcc "lib") jack-1 pulseaudio wdl))
    (supported-systems
      (list "x86_64-linux" "i686-linux" "aarch64-linux" "armhf-linux"))
    (home-page "https://www.reaper.fm")
    (synopsis "Digital audio workstation")
    (description
     "REAPER is a digital audio production application offering multitrack
audio and MIDI recording, editing, processing, mixing and mastering toolset.
It supports a vast range of hardware, digital formats and plugins, and can be
comprehensively extended, scripted and modified.")
    (license (license:nonfree "file:///opt/REAPER/EULA.txt"))))

(define-public bitwig-studio
  (package
    (name "bitwig-studio")
    (version "5.0.4")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://downloads.bitwig.com/" version
                                  "/bitwig-studio-" version ".deb"))
              (sha256
               (base32
                "15hk8mbyda0isbqng00wd0xcp8g91117lkg6f5b2s0xylf858j12"))))
    (build-system binary-build-system)
    (arguments
     (list #:install-plan
           #~`(("opt" "opt"
               #:exclude ("BitwigPluginHost-X86-SSE41")) ;no multilib
               ("usr/share" "share"))
           #:phases
           #~(modify-phases %standard-phases
               (replace 'unpack
                 (lambda _
                   (invoke "ar" "x" #$source)
                   (invoke "rm" "-v" "control.tar.xz" "debian-binary")
                   (invoke "tar" "xf" "data.tar.xz")
                   (invoke "rm" "-vrf" "data.tar.xz")))
               (add-after 'unpack 'unbundle-deps
                 (lambda _
                   (with-directory-excursion "opt/bitwig-studio"
                     (with-directory-excursion "lib"
                       (for-each delete-file-recursively
                        (list "cp"
                              "jre")))
                     (for-each delete-file
                      (list "bin/BitwigStudio"
                            "bin/ffmpeg"
                            "bin/ffprobe"
                            "bin/libLTO.so"
                            ;FIXME: Unbundle "bin/liblwjgl.so"
                            "bin/libxcb-imdkit.so"
                            "lib/bitwig-studio/libxcb-imdkit.so.1"))
                     (symlink (string-append #$openjdk17 "/bin/java")
                      "bin/BitwigStudio"))))
               (replace 'patchelf
                 (lambda* (#:key inputs #:allow-other-keys)
                   (with-directory-excursion "opt/bitwig-studio"
                     (invoke "patchelf" "--set-interpreter"
                             (string-append (assoc-ref inputs "libc")
                                            "/lib/ld-linux-x86-64.so.2")
                             "bitwig-studio"
                             "BitwigStudio"
                             "bin/BitwigAudioEngine-X64-AVX2"
                             "bin/BitwigAudioEngine-X64-SSE41"
                             "bin/BitwigPluginHost-X64-SSE41"
                             "bin/BitwigVampHost"
                             "bin/show-file-dialog-gtk3"))))
               (add-after 'install 'create-wrapper
                 (lambda* (#:key inputs #:allow-other-keys)
                   (make-wrapper
                     (string-append #$output "/bin/bitwig-studio")
                     (string-append #$output
                      "/opt/bitwig-studio/bitwig-studio")
                     #:skip-argument-0? #t
                     `("PATH" suffix
                       (,(string-append #$ffmpeg "/bin")))
                     `("LD_LIBRARY_PATH" ":" suffix
                       (,(string-append (assoc-ref inputs "libc") "/lib")
                        ,(string-append (assoc-ref inputs "gcc") "/lib")
                        ,(string-append #$alsa-lib "/lib")
                        ,(string-append #$cairo "/lib")
                        ,(string-append #$freetype "/lib")
                        ,(string-append #$gdk-pixbuf "/lib")
                        ,(string-append #$glib "/lib")
                        ,(string-append #$gtk+ "/lib")
                        ,(string-append #$jack-1 "/lib")
                        ,(string-append #$libusb4java "/lib")
                        ,(string-append #$libx11 "/lib")
                        ,(string-append #$libxcb "/lib")
                        ,(string-append #$libxcursor "/lib")
                        ,(string-append #$libxkbcommon "/lib")
                        ,(string-append #$llvm-13 "/lib")
                        ,(string-append #$mesa "/lib")
                        ,(string-append #$pipewire "/lib")
                        ,(string-append #$pulseaudio "/lib")
                        ,(string-append #$xcb-imdkit "/lib")
                        ,(string-append #$xcb-util "/lib")
                        ,(string-append #$xcb-util-wm "/lib")
                        ,(string-append #$zlib "/lib"))))))
               (replace 'install-license-files
                 (lambda _
                   (install-file
                    (string-append "opt/bitwig-studio/EULA.txt")
                    (string-append #$output "/share/doc/"
                     (strip-store-file-name #$output))))))))
    (inputs
     (list alsa-lib
           cairo
           ffmpeg
           freetype
           `(,gcc "lib")
           gdk-pixbuf
           glib
           gtk+
           jack-1
           libusb4java
           libx11
           libxcb
           libxcursor
           libxkbcommon
           llvm-13
           mesa
           openjdk17
           pipewire
           pulseaudio
           xcb-imdkit
           xcb-util
           xcb-util-wm
           zlib))
    (supported-systems
     (list "x86_64-linux"))
    (home-page "https://www.bitwig.com/")
    (synopsis "Music production and performance software")
    (description
     "Bitwig Studio is a digital audio workstation which is aimed at loop based
live performance and production.  It features modulations, macros, note
expressions, LFOs and envelopes.  It also supports VST plugins and hardware
integration.")
    (license (license:nonfree "file:///opt/bitwig-studio/EULA.txt"))))

(define-public stk
  (package
    (name "stk")
    (version "5.0.1")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://ccrma.stanford.edu/software/stk/"
                                  "release/stk-" version ".tar.gz"))
              (sha256
               (base32
                "1151cpapg8vc8g2sldkgsj6psksyfkxb77cdrg5am2xvlfp5zhxg"))
              (patches
               (list
                (local-file "patches/stk-5.0.1-typo.patch")))
              (modules '((guix build utils)))
              (snippet
               '(begin
                  ;; Fix commands.
                  (substitute* (find-files "." "Makefile\\.in")
                    (("/bin/") ""))))))
    (build-system gnu-build-system)
    (arguments
     (list #:tests? #f ;no tests
           #:configure-flags
           #~(list (string-append "RAWWAVE_PATH=" #$output
                                  "/share/stk/rawwaves/"))
           #:phases
           #~(modify-phases %standard-phases
               (add-after 'unpack 'fix-paths
                 (lambda _
                   (with-directory-excursion "projects"
                     (for-each (lambda (file)
                                 (substitute* (string-drop-right file 4)
                                   (("wish") (which "wish"))
                                   (("< tcl") (string-append "< " #$output:gui
                                                             "/share/stk/tcl"))
                                   (("\\./") (string-append #$output "/bin/"))))
                      (find-files "." "\\.bat$"))
                     (substitute* (find-files "share/stk/tcl" "\\.tcl$")
                       (("tcl/bitmaps")
                        (string-append #$output:gui "/share/stk/tcl/bitmaps")))
                     (substitute* (find-files "." "\\.cpp$")
                       (("\\.\\./\\.\\./rawwaves")
                        (string-append #$output "/share/stk/rawwaves"))
                       (("\"rawwaves")
                        (string-append "\"" #$output "/share/stk/rawwaves"))))))
               (add-after 'install 'install-data
                 (lambda _
                   (let* ((bin (string-append #$output "/bin"))
                          (data (string-append #$output "/share/stk"))
                          (rawwaves (string-append data "/rawwaves"))
                          (scores (string-append data "/scores"))
                          (gui (string-append #$output:gui "/bin"))
                          (tcl (string-append #$output:gui "/share/stk/tcl")))
                     (mkdir-p data)
                     ;; Install rawwaves.
                     (copy-recursively "rawwaves" rawwaves)
                     ;; Install projects.
                     (with-directory-excursion "projects"
                       ;; Install project binaries.
                       (for-each (lambda (file)
                                   (install-file file bin))
                        (list "demo/stk-demo"
                              "effects/effects"
                              "examples/audioprobe"
                              "examples/bethree"
                              "examples/controlbee"
                              "examples/crtsine"
                              "examples/duplex"
                              "examples/foursine"
                              "examples/grains"
                              "examples/inetIn"
                              "examples/inetOut"
                              "examples/midiprobe"
                              "examples/play"
                              "examples/playsmf"
                              "examples/record"
                              "examples/rtsine"
                              "examples/sine"
                              "examples/sineosc"
                              "examples/threebees"
                              "eguitar/eguitar"
                              "ragamatic/ragamat"))
                       ;; Install project rawwaves.
                       (for-each (lambda (dir)
                                   (copy-recursively dir rawwaves))
                        (list "examples/rawwaves"
                              "ragamatic/rawwaves"))
                       ;; Install project scores.
                       (for-each (lambda (dir)
                                   (copy-recursively dir scores))
                        (list "demo/scores"
                              "eguitar/scores"
                              "examples/scores"))
                       ;; Install GUI scripts.
                       (for-each (lambda (file)
                                   (install-file (string-drop-right file 4) gui))
                        (find-files "." "\\.bat"))
                       ;; Install TCL files
                       (for-each (lambda (dir)
                                   (copy-recursively dir tcl))
                        (list "demo/tcl"
                              "effects/tcl"
                              "eguitar/tcl"
                              "ragamatic/tcl")))))))))
    (outputs
     '("out" "gui"))
    (inputs
     (list alsa-lib jack-2 tk))
    (home-page "https://ccrma.stanford.edu/software/stk/")
    (synopsis "Audio signal processing and algorithmic synthesis classes")
    (description
     "Synthesis ToolKit in C++ (STK) is a set of audio signal processing and
algorithmic synthesis classes written in C++.

This package also provides its demo project, examples, ElectricGuitar,
RagaMatic and Effects.")
    (license (license:non-copyleft "file:///LICENSE"))))

;;; This package variant tracks the latest in-development 1.3 release.
(define-public lmms-1.3.0-alpha.1-d973788
  (let ((commit "d9737881cf482e89969948f56da954cbf5c11aca") ;commit from master
        (revision "0"))
    (package/inherit lmms
      (version (git-version "1.3.0-alpha.1" revision commit))
      (source
       (origin
         (inherit (package-source lmms))
         (uri (git-reference
                (url "https://github.com/LMMS/lmms")
                (commit commit)
                ;; Clone recursively for optional plugins.
                (recursive? #t)))
         (file-name (git-file-name (package-name lmms) version))
         (sha256
          (base32 "0hscgnbladrkbvv551bv7vzi5949gjhf397j35wghd07hwd2lrb0"))
         (modules '((guix build utils)))
         (snippet
          '(begin
             ;; Delete the bundled 3rd party libraries.
             (for-each delete-file-recursively
               (list "plugins/CarlaBase/carla"
                     "plugins/OpulenZ/adplug"
                     "plugins/Xpressive/exprtk"
                     "src/3rdparty"))
             ;; Do not check submodules.
             (substitute* "CMakeLists.txt"
               ((".*CheckSubmodules.*") "")
               ;; Unuse the bundled weakjack.
               (("JACK libraries\" ON") "JACK libraries\" OFF"))
             (substitute* "src/CMakeLists.txt"
               ;; Do not add 3rd party libraries.
               ((".*3rdparty.*") "")
               ;; Use adplug, qt5-x11embed and ringbuffer from the system.
               (("\\$\\{EXTRA_LIBRARIES\\}")
                (string-append "${EXTRA_LIBRARIES}\n"
                               "\tadplug\n"
                               "\tqx11embedcontainer\n"
                               "\tringbuffer")))
             (with-directory-excursion "plugins"
               ;; Use carla from the system.
               (substitute* "CarlaBase/Carla.h"
                 (("#include <CarlaDefines")
                  "#include <carla/includes/CarlaDefines")
                 (("#include <CarlaNative")
                  "#include <carla/includes/CarlaNative")
                 (("#include <CarlaBackend") "#include <carla/CarlaBackend")
                 (("#include <CarlaUtils") "#include <carla/CarlaUtils"))
               (with-directory-excursion "OpulenZ"
                 ;; Unuse the bundled adplug.
                 (substitute* "CMakeLists.txt"
                   ((".*adplug .*") "")
                   ((".*adplug/.*") "")
                   (("^\\)") "")
                   (("\\.png\"") ".png\"\n)"))
                 ;; Use adplug from the system.
                 (substitute* "OpulenZ.cpp"
                   (("<opl\\.h>") "<adplug/opl.h>")
                   (("<temuopl\\.h>") "<adplug/temuopl.h>")
                   (("<mididata\\.h>") "<adplug/mididata.h>")))
               ;; Unuse the bundled exprtk.
               (substitute* "Xpressive/CMakeLists.txt"
                 ((".*\\(exprtk .*") "")
                 ((".*xpressive exprtk.*") "")))))))
      (build-system qt-build-system)
      (arguments
       (substitute-keyword-arguments (package-arguments lmms)
         ((#:qtbase qtbase) qtbase)
         ((#:configure-flags flags)
          #~(list (string-append "-DWINE_LOCATIONS="
                                 (if (or #$(target-x86-64?)
                                         #$(target-aarch64?))
                                   #$(this-package-input "wine64")
                                   (if (or #$(target-x86-32?)
                                           #$(target-arm32?))
                                     #$(this-package-input "wine")
                                     "")))))
         ((#:phases phases)
          #~(modify-phases #$phases
              (delete 'unpack-rpmalloc)
              (add-after 'unpack 'patch-rawwaves-path
                (lambda* (#:key inputs #:allow-other-keys)
                  (substitute* "src/core/ConfigManager.cpp"
                    (("/usr") (assoc-ref inputs "stk")))))))))
      (inputs
       (modify-inputs (package-inputs lmms)
         (delete "rpmalloc")
         (replace "carla" carla-2.6.0-17000e7-qt5)
         (replace "sdl12-compat" sdl2)
         (append adplug
                 exprtk
                 lame
                 libgig
                 libsoundio
                 libxml2
                 lilv
                 lv2
                 perl
                 perl-list-moreutils
                 perl-xml-parser
                 pulseaudio
                 qt5-x11embed
                 qtwayland-5
                 ringbuffer
                 stk
                 suil
                 ;; Only install wine or wine64 on the platforms they support
                 ;; to enable VST support, so that on other platforms this
                 ;; package can be built without it.
                 (match (%current-system)
                   ((or "x86_64-linux" "aarch64-linux") wine64)
                   ((or "i686-linux" "armhf-linux") wine)
                   (_ '())))))
      (native-search-paths
       (list (search-path-specification
              (variable "LV2_PATH")
              (files '("lib/lv2"))))))))

(define-public lmms-1.3.0-alpha.1-9de3f3b-qt6
  (let ((commit "9de3f3b415f58aa95f7a9fa25ed751bea477b929") ;commit from qt6
        (revision "0"))
    (package/inherit lmms-1.3.0-alpha.1-d973788
      (name "lmms-qt6")
      (version (git-version "1.3.0-alpha.1" revision commit))
      (source
       (origin
         (inherit (package-source lmms-1.3.0-alpha.1-d973788))
         (uri (git-reference
                (url "https://github.com/LMMS/lmms")
                (commit commit)
                ;; Clone recursively for weakjack, ringbuffer, resid and the
                ;; lmms plugins that are not present in this repository. These
                ;; submodules are required for building this package.
                (recursive? #t)))
         (file-name (git-file-name (package-name lmms) version))
         (sha256
          (base32 "0857yakkrlbdhs8sx552hkrpx6jyq1li845r9r10pi2831fxkh44"))))
      (arguments
       (substitute-keyword-arguments (package-arguments
                                      lmms-1.3.0-alpha.1-d973788)
         ((#:qtbase qtbase) qtbase)
         ((#:configure-flags flags)
          #~(append (list "-DWANT_QT6=ON") #$flags))
         ((#:phases phases)
          #~(modify-phases #$phases
              (add-after 'unpack 'remove-qt5-x11embed
                (lambda _
                  (substitute* "src/CMakeLists.txt"
                    (("qx11embedcontainer") ""))))))))
      (native-inputs
       (modify-inputs (package-native-inputs lmms-1.3.0-alpha.1-d973788)
         (replace "qttools" qttools)))
      (inputs
       (modify-inputs (package-inputs lmms-1.3.0-alpha.1-d973788)
         (delete "qt5-x11embed" "qtx11extras")
         (replace "qtwayland" qtwayland)
         (replace "carla" carla-2.6.0-17000e7)
         (replace "qtbase" qtbase))))))

(define-public zynaddsubfx
  (package
    (name "zynaddsubfx")
    (version "3.0.6")
    (source (origin
              (method url-fetch)
              (uri (string-append
                    "mirror://sourceforge/zynaddsubfx/zynaddsubfx/"
                    version "/zynaddsubfx-" version ".tar.bz2"))
              (sha256
               (base32
                "1bkirvcg0lz1i7ypnz3dyh218yhrqpnijxs8n3wlgwbcixvn1lfb"))
              (patches
               (list
                (local-file "patches/zynaddsubfx-3.0.6-system-rtosc.patch")))))
    (build-system cmake-build-system)
    (arguments
     `(#:configure-flags `("-DGuiModule=zest"
                         ,(string-append "-DZYN_DATADIR="
                                         (assoc-ref %outputs "out")
                                         "/share/zynaddsubfx")
                         "-DZYN_SYSTEM_RTOSC=ON")
       #:phases
       (modify-phases %standard-phases
         ;; Move SSE compiler optimization flags from generic target to
         ;; athlon64 and core2 targets, because otherwise the build would fail
         ;; on non-Intel machines.
         (add-after 'unpack 'remove-sse-flags-from-generic-target
          (lambda _
            (substitute* "src/CMakeLists.txt"
              (("-msse -msse2 -mfpmath=sse") "")
              (("-march=(athlon64|core2)" flag)
               (string-append flag " -msse -msse2 -mfpmath=sse")))))
         (add-after 'unpack 'patch-paths
           (lambda* (#:key inputs #:allow-other-keys)
             (substitute* "src/main.cpp"
               (("\\./zyn-fusion")
                (search-input-file inputs "/bin/zyn-fusion")))
             (substitute* "src/Plugin/ZynAddSubFX/ZynAddSubFX-UI-Zest.cpp"
               (("\\./libzest\\.so")
                (search-input-file inputs "/lib/libzest.so"))))))))
    (inputs
     (list liblo
           ntk
           rtosc
           mruby-zest
           mesa
           alsa-lib
           jack-1
           fftw
           fftwf
           minixml
           libxpm
           zlib))
    (native-inputs
     (list pkg-config
           ruby))
    (native-search-paths
     (list (search-path-specification
            (variable "LV2_PATH")
            (files '("lib/lv2")))
           (search-path-specification
            (variable "VST2_PATH")
            (files '("lib/vst")))))
    (home-page "https://zynaddsubfx.sourceforge.io/")
    (synopsis "Software synthesizer")
    (description
     "ZynAddSubFX is a feature heavy realtime software synthesizer.  It offers
three synthesizer engines, multitimbral and polyphonic synths, microtonal
capabilities, custom envelopes, effects, etc.")
    (license license:gpl2)))
