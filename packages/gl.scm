;;; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (gl)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix utils)
  #:use-module (guix build-system gnu)
  #:use-module ((guix licenses) #:prefix license:))

(define-public nanovg
  ;; No tags are available.
  (let ((revision "0")
        (commit "7544c114e83db7cf67bd1c9e012349b70caacc2f"))
    (package
      (name "nanovg")
      (version (git-version "0" revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://github.com/memononen/nanovg")
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32
                  "0n29rxdp20xnvnpsjcmg3v5n6z7ya5ji4qirp18qcyls5xdxdxkc"))))
      (build-system gnu-build-system)
      (arguments
       (list #:tests? #f ;no test suite
             #:phases
             #~(modify-phases %standard-phases
                 (delete 'configure) ;no configure script
                  (replace 'build
                    (lambda _
                      (invoke #$(cc-for-target)
                       "../source/src/nanovg.c" "-c" "-fPIC")
                      (invoke #$(cc-for-target)
                       "-shared" "-olibnanovg.so" "nanovg.o")))
                  (replace 'install
                    (lambda _
                      (let ((lib (string-append #$output "/lib"))
                            (include (string-append #$output
                                                    "/include")))
                        (install-file "libnanovg.so" lib)
                        (with-directory-excursion "../source"
                          (for-each
                            (lambda (file)
                              (install-file file include))
                            (find-files "src" "nanovg.*\\.h$")))))))))
      (home-page "https://github.com/memononen/nanovg")
      (synopsis "2D vector drawing library on top of OpenGL")
      (description
       "NanoVG is an antialiased vector graphics rendering library for OpenGL.
It is aimed for building scalable user interfaces and visualizations.")
      (license license:zlib))))
