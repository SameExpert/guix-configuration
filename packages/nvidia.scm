;;; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (nvidia)
  #:use-module (guix packages)
  #:use-module (nongnu packages nvidia))

(define-public nvda32
  (package/inherit nvda
    (name "nvda32")
    (arguments
     (append
       (list #:system "i686-linux")
       (package-arguments nvda)))
    (description
     "This package provides 32-bit version of the original @code{nvda} package.
Avoid installing this in a profile in which the original package is
installed.")))
