;;; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (wine-xyz)
  #:use-module (gnu packages wine)
  #:use-module (guix download)
  #:use-module (guix build-system copy)
  #:use-module (guix gexp)
  #:use-module ((guix licenses) :prefix license:)
  #:use-module (guix packages)
  #:use-module (guix utils))

(define-public wine-mono
  (package
    (name "wine-mono")
    (version "8.0.0")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "https://dl.winehq.org/wine/wine-mono/" version
                           "/wine-mono-" version "-x86.tar.xz"))
       (sha256
        (base32
         "0xvsrhl22yzf9mljwvj0n8mk7ynwgag7blfriqnwd7dph1kxgiql"))))
    (build-system copy-build-system)
    (arguments
     (list #:install-plan
           #~(list (list "." "share/wine/mono/"))))
    (home-page "https://wiki.winehq.org/Mono")
    (synopsis "Implementation of the .NET Framework")
    (description
     "Mono is an implementation of the .NET Framework for Wine.")
    (license license:expat)))

(define-public wine-gecko
  (let ((arch (if (target-64bit?) "x86_64" "x86")))
    (package
      (name "wine-gecko")
      (version "2.47.4")
      (source
       (origin
         (method url-fetch)
         (uri (string-append "https://dl.winehq.org/wine/wine-gecko/" version
                             "/wine-gecko-" version "-" arch ".tar.xz"))
         (sha256
          (base32
           "0518m084f9bdl836gs3d8qm8jx65j2y1w35zi9x8s1bxadzgr27x"))))
      (build-system copy-build-system)
      (arguments
       (list #:install-plan
             #~(list (list "." "share/wine/gecko/"))))
      (home-page "https://wiki.winehq.org/Gecko")
      (synopsis "Internet explorer for Wine")
      (description
       "Gecko is an internet explorer for Wine.")
      (license license:expat))))
