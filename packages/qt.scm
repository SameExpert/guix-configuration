;;; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (qt)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system qt)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages kde-frameworks)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages xdisorg))

(define-public qcodeeditor
  (let ((commit "dc644d41b68978ab9a5591ba891a223221570e74")
        (revision "0"))
    (package
      (name "qcodeeditor")
      (version (git-version "0" revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://github.com/Megaxela/QCodeEditor")
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32
                  "1bpvfwbgp275w79dzrd7d9k3md1ch7n88rh59mxdfj8s911n42j8"))))
      (build-system qt-build-system)
      (arguments
       (list #:tests? #f ;no tests
             #:configure-flags
             #~(list "-DBUILD_EXAMPLE=ON")
             #:phases
             #~(modify-phases %standard-phases
                 (replace 'install
                   (lambda _
                     (install-file "example/QCodeEditorExample"
                                   (string-append #$output "/bin"))
                     (install-file "libQCodeEditor.a"
                                   (string-append #$output "/lib"))
                     (for-each
                       (lambda (file)
                         (install-file file
                                       (string-append #$output
                                                      "/include/QCodeEditor")))
                       (find-files "../source/include/internal" "\\.hpp")))))))
      (inputs
       (list qtwayland-5))
      (home-page "https://github.com/Megaxela/QCodeEditor")
      (synopsis "Qt code editor widget")
      (description
       "QCodeEditor is a Qt widget for editing/viewing code.")
      (license license:expat))))

(define-public qt-advanced-docking-system
  (package
    (name "qt-advanced-docking-system")
    (version "4.3.1")
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
          (url "https://github.com/githubuser0xFFFF/Qt-Advanced-Docking-System")
          (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0xb4j5pva3qbbj01xp5y98544hgh14p60z2dfypq4ikz6n3ac0z7"))))
    (build-system qt-build-system)
    (arguments
     (list #:qtbase qtbase
           #:tests? #f ;no tests
           #:configure-flags
           #~(list ;; Examples requie qtdeclarative. Even if they are built,
                   ;; they fail to find the shared library of this package.
                   "-DBUILD_EXAMPLES=OFF"
                   #$(string-append "-DADS_VERSION=" version))))
    (inputs
     (list libxkbcommon))
    (home-page "https://github.com/githubuser0xFFFF/Qt-Advanced-Docking-System")
    (synopsis "Advanced docking system for Qt")
    (description
     "Qt Advanced Docking System lets you create customizable layouts using a
window docking system.")
    (license license:lgpl2.1+)))

(define-public qtcsv
  (package
    (name "qtcsv")
    (version "1.7")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/iamantony/qtcsv")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1c9i93kr7wvpr01i4wixi9mf991nd3k2adg5fy0vxwwlvvc7dgdw"))))
    (build-system qt-build-system)
    (arguments
     (list #:qtbase qtbase
           #:test-target "tests"))
    (home-page "https://github.com/iamantony/qtcsv")
    (synopsis "Library for reading and writing csv-files in Qt")
    (description
     "qtcsv is a library for reading and writing csv-files in Qt.")
    (license license:expat)))

(define-public qtpromise
  (package
    (name "qtpromise")
    (version "0.7.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/simonbrunel/qtpromise")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0nsh6v5k4kdrrhcd6adz947n0dka4rrbx8f8rvm1175545nbi67s"))))
    (build-system qt-build-system)
    (arguments
     (list #:test-target "tests"
           #:phases
           #~(modify-phases %standard-phases
               (add-before 'install 'fix-include-path
                 (lambda _
                   (chdir "../source")
                   (substitute* "../source/include/QtPromise"
                     (("../src/") ""))))
               (replace 'install
                 (lambda _
                   (let ((include (string-append #$output "/include")))
                     (with-directory-excursion "../source"
                       (install-file "include/QtPromise"
                                     (string-append include))
                       (copy-recursively "src/qtpromise"
                                         (string-append include
                                                        "/qtpromise")))))))))
    (home-page "https://qtpromise.netlify.app/")
    (synopsis "Promises/A+ implementation for Qt/C++")
    (description
     "This package provides Promises/A+ implementation for Qt/C++.")
    (license license:expat)))

(define-public qt-widget-animation-framework
  (let ((commit "b07ab59cee7a21eb29d29cb67c160681f13ac5ae") ;no tags
          (revision "0"))
    (package
      (name "qt-widget-animation-framework")
      (version (git-version "0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri
           (git-reference
             (url "https://github.com/dimkanovikov/WidgetAnimationFramework")
             (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "1smbdrzk05vvbf6lpjdq82k4y2kc4yv1gk5388qbslbzlb6ihls6"))
         (modules '((guix build utils)))
         (snippet
          ;; This project does not have any build rule but its demo has one. So
          ;; make use of it.
          '(begin
             (copy-file "demo/waf-demo.pro" "src/waf.pro")
             (substitute* "src/waf.pro"
               (("main.cpp ") "")
               (("app") "lib")
               (("waf-demo") "waf"))))))
      (build-system qt-build-system)
      (arguments
       (list #:qtbase qtbase
             #:tests? #f ;no tests
             #:phases
             #~(modify-phases %standard-phases
                 (replace 'configure
                   (lambda _
                     (chdir "src")
                     (invoke "qmake")))
                 (replace 'install
                   (lambda _
                     ;; Install library files.
                     (for-each
                       (lambda (file)
                         (install-file file
                                       (string-append #$output "/lib/"
                                                      (dirname file))))
                       (find-files "." "\\.so"))
                     ;; Install header files.
                     (for-each
                       (lambda (file)
                         (install-file file
                                       (string-append #$output "/include/WAF/"
                                                      (dirname file))))
                       (find-files "." "\\.h$")))))))
      (home-page "https://github.com/dimkanovikov/WidgetAnimationFramework")
      (synopsis "Extension for animating Qt widgets")
      (description
       "Widget Animation Framework (WAF) is an extension for animating Qt
widgets.")
      (license license:lgpl3))))

(define-public qt5-x11embed
  (let ((commit "5337eefc06feaa0558ea40c43db0b749fdc8ee64") ;no tags
        (revision "0"))
    (package
      (name "qt5-x11embed")
      (version (git-version "0" revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://github.com/Lukas-W/qt5-x11embed")
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32
                  "0m7n857hxb9cq97x6a3c5vkpgx660vr3fmqhzh6v2l65ccxqpgkp"))
                (patches
                 (list (local-file "patches/qt5-x11embed-pkgconfig.patch")))))
      (build-system qt-build-system)
      (arguments
       (list #:tests? #f ;no tests
             #:configure-flags
             #~(list "-DBUILD_SHARED_LIBS=ON")
             #:phases
             #~(modify-phases %standard-phases
                 (add-after 'install 'install-header
                   (lambda _
                     (install-file "../source/src/X11EmbedContainer.h"
                                   (string-append #$output "/include")))))))
      (native-inputs
       (list extra-cmake-modules pkg-config))
      (inputs
       (list libx11 libxcb qtx11extras xcb-util xcb-util-keysyms))
      (home-page "https://github.com/libsidplayfp/resid")
      (synopsis " XEmbed container widget")
      (description
       "This is a port of QX11EmbedContainer from Qt4 to Qt5.

The @code{QX11EmbedContainer} class provides an XEmbed container widget.
XEmbed is an X11 protocol that supports the embedding of a widget from one
application into another application.  An XEmbed container is the graphical
location that embeds an external client widget.  A client widget is a window
that is embedded into a container.")
    (license license:lgpl2.1+))))
