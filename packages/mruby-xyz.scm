;;; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (mruby-xyz)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix gexp)
  #:use-module (guix utils)
  #:use-module (guix build-system gnu)
  #:use-module (gnu packages)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages fonts)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages libevent)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages ruby)
  #:use-module (gnu packages xorg)
  #:use-module (audio)
  #:use-module (gl))

(define-public mruby-zest
  (package
    (name "mruby-zest")
    (version "3.0.6")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/mruby-zest/mruby-zest-build")
                    (commit version)
                    ;; Cloning recursively for and the required mruby sources
                    ;; and gems.
                    (recursive? #t)))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0dz4zv1km9805lji2q2qqdd8s8hgfd723dxdzcivbhm612szm1mc"))
              (modules '((guix build utils)))
              (snippet
               ;; This packages contains a custom "pugl". Replacing it with the
               ;; system "pugl" fails to build.
               ;; Delete the bundled "libuv", "nanovg" and "rtosc".
               '(with-directory-excursion "deps"
                  (for-each delete-file-recursively
                   (list "libuv" "nanovg" "rtosc"))))))
    (build-system gnu-build-system)
    (arguments
     (list #:tests? #f ;no test suite
           #:make-flags
           #~(list (string-append "CC=" #$(cc-for-target))
                    "CONFIG_SHELL=bash")
           #:phases
           #~(modify-phases %standard-phases
               (delete 'configure) ;no configure script
               (add-after 'unpack 'patch-paths
                 (lambda* (#:key inputs #:allow-other-keys)
                   ;; Do not compile "nanovg" and "libuv" and use the system
                   ;; provided versions of them.
                   (substitute* "Makefile"
                     (("cd deps/nanovg") "#cd deps/nanovg")
                     (("\\$\\(AR\\) rc deps/libnanovg\\.a")
                      "#$(AR) rc deps/libnanovg.a")
                     (("\\./deps/libnanovg\\.a")
                      "-lnanovg")
                     (("cd deps/libuv") "#cd deps/libuv")
                     (("\\./deps/libuv/\\.libs/libuv\\.a")
                      "-luv")
                     (("\\./deps/libuv/\\.libs/libuv-win\\.a")
                      "-luv-win"))
                   (substitute* "build_config.rb"
                     (("#\\{`pwd`\\.strip\\}/\\.\\./deps/libnanovg\\.a")
                      "-lnanovg")
                     (("#\\{`pwd`\\.strip\\}/\\.\\./deps/libuv\\.a")
                      "-luv")
                     (("cc\\.include_paths.*\\./deps/nanovg/.*$")
                      "#")
                     (("cc\\.include_paths.*\\./deps/libuv/.*$")
                      "#"))
                   (substitute* "deps/mruby-nanovg/src/nvg_screenshot.c"
                    (("\\.\\./\\.\\./nanovg/.*/stb_image_write\\.h")
                     (search-input-file inputs "/example/stb_image_write.h")))
                   ;; Use the system provided "rtosc" headers.
                   (substitute* "src/mruby-widget-lib/src/gem.c"
                     (("\"\\.\\./\\.\\./\\.\\./deps/.*/rtosc\\.h\"")
                      "<rtosc/rtosc.h>"))
                   ;; Fix missing fonts.
                   (substitute*
                    "src/mruby-widget-lib/mrblib/script.rb"
                     (("sans = \\[")
                      (string-append
                       "sans = [\""
                       (search-input-file inputs
                        "/share/fonts/truetype/Roboto-Regular.ttf")
                        "\", "))
                     (("bold = \\[")
                      (string-append
                       "bold = [\""
                       (search-input-file inputs
                        "/share/fonts/truetype/Roboto-Bold.ttf")
                        "\", ")))
                   ;; Fix paths for zyn-fusion files.
                   (substitute* "src/osc-bridge/src/bridge.c"
                     (("fopen\\(\\\"schema/")
                      (string-append
                       "fopen(\"" #$output "/share/zyn-fusion/schema/")))
                   (substitute* "test-libversion.c"
                     (("./libzest.so")
                      (string-append #$output "/lib/libzest.so")))
                   (substitute* "src/mruby-widget-lib/src/api.c"
                     ((", \\\"\\./qml/") ", \"../share/zyn-fusion/qml/")
                     (("\\./qml/")
                      (string-append #$output "/share/zyn-fusion/qml")))))
               (replace 'install
                 (lambda _
                   (rename-file "zest" "zyn-fusion")
                   (install-file "zyn-fusion" (string-append #$output "/bin"))
                   (install-file "libzest.so" (string-append #$output "/lib"))
                   (for-each
                     (lambda (folder)
                       (for-each
                         (lambda (file)
                           (install-file file
                            (string-append #$output "/share/zyn-fusion/qml")))
                         (find-files folder "\\.qml$")))
                     '("src/mruby-zest/qml" "src/mruby-zest/example"))
                   (install-file "src/osc-bridge/schema/test.json"
                    (string-append #$output "/share/zyn-fusion/schema"))
                   (install-file "completions/zyn-fusion"
                    (string-append
                     #$output "/share/bash-completion/completions")))))))
    (native-inputs
     (list (package-source nanovg) ;for "stb_image_write.h"
           pkg-config
           ruby))
    (inputs
     (list font-google-roboto
           libuv
           libx11
           mesa
           ;; With the latest version of "nanovg" the contents are not
           ;; displayed properly.
           (let ((version "0")
                 (revision "0")
                 (commit "b83cf926525e7cea8d2483da2a75852b8c7b6d28"))
             (package (inherit nanovg)
               (version (git-version version revision commit))
               (source
                (origin
                  (method git-fetch)
                  (uri (git-reference
                         (url "https://github.com/memononen/nanovg")
                         (commit commit)))
                  (sha256
                   (base32
                    "1zp355pw6xnb5mjarz67cxkglv7j1s0qzv705fz1wf0gl0sjhy8f"))))))
           rtosc))
    (home-page "https://github.com/mruby-zest/mruby-zest-build")
    (synopsis "Zyn-Fusion user interface")
    (description
     "This package provides user interface for Zyn-Fusion.")
    (license
     (list license:lgpl2.1))))
