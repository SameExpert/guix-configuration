;;; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (disk)
  #:use-module (gnu packages compression)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (ice-9 match)
  #:use-module (nonguix build-system binary)
  #:use-module (nonguix licenses))

(define-public dmde
  (let ((arch (match (or (%current-target-system) (%current-system))
                     ("x86_64-linux" "64")
                     ("i686-linux" "32"))))
  (package
    (name "dmde")
    (version "4.0.6.806")
    (source
      (origin
        (method url-fetch)
        (uri (string-append "https://dmde.com/download/dmde-"
                            (string-replace-substring version "." "-") "-lin"
                            arch "-con.zip"))
        (sha256
          (base32
           (match arch
             ("64" "06n01kggc1hrgjwpqfl4wqgwm8j46wp1n40q15rwd81hnkdvn2gd")
             ("32" "0mxyaxb44qhb75wqir8hvab9ignzcps8m3y5ibj1j3dvkhmi0nrl"))))))
    (build-system binary-build-system)
    (arguments
     (list #:patchelf-plan
           #~`(("dmde" ("libc")))
           #:install-plan
           #~`(("." "opt/dmde"))
           #:phases
           #~(modify-phases %standard-phases
               (replace 'unpack
                 (lambda* (#:key source #:allow-other-keys)
                     (invoke "unzip" source "-d" "source")
                     (chdir "source")))
               (add-after 'install 'create-wrapper
                 (lambda _
                   (make-wrapper
                     (string-append #$output "/sbin/dmde")
                     (string-append #$output "/opt/dmde/dmde"))))
               (replace 'install-license-files
                 (lambda _
                   (install-file "eula.txt"
                    (string-append #$output "/share/doc/"
                      (strip-store-file-name #$output))))))))
    (native-inputs
     (list unzip))
    (supported-systems
     (list "i686-linux" "x86_64-linux"))
    (home-page "https://dmde.com/")
    (synopsis "Disk editor and data recovery software")
    (description
     "DMDE is a tool for data searching, editing, and recovery on disks.")
     (license (nonfree "file:///eula.txt")))))
