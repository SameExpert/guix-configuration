;;; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (fonts)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix download)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system font)
  #:use-module ((guix licenses) #:prefix license:))

(define-public fontconfig-google-noto-emoji
  (package
    (name "fontconfig-google-noto-emoji")
    (version "1.0.0-2")
    (source
     (origin
       (method url-fetch)
       (uri
        (string-append "https://aur.archlinux.org/cgit/aur.git/plain/"
                       "75-noto-color-emoji.conf"
                       "?h=noto-color-emoji-fontconfig"))
       (file-name "75-noto-color-emoji.conf")
       (sha256
        (base32 "0hk90l1zkmnl2481schqyjrd4swaj9yws6cgzhm5i20giq3qwzcv"))))
    (build-system copy-build-system)
    (arguments
     (list #:install-plan
           #~`(("75-noto-color-emoji.conf" "share/fontconfig/conf.avail/")
               ("75-noto-color-emoji.conf" "share/fontconfig/conf.d/"))))
    (home-page
     "https://aur.archlinux.org/packages/noto-color-emoji-fontconfig")
    (synopsis "Fontconfig to enable Google Noto Emoji")
    (description
     "This package provides fontconfig file to enable Google Noto Emoji fonts
where emojis can be displayed.")
    (license #f)))

(define-public font-fira-code-nerd
  (package
    (name "font-fira-code-nerd")
    (version "3.0.2")
    (source
     (origin
       (method url-fetch)
       (uri
        (string-append "https://github.com/ryanoasis/nerd-fonts/"
                       "releases/download/v" version "/FiraCode.zip"))
       (sha256
        (base32 "1hn19sigsv6i1dm5lxn0gfldqfcn9yvzhg5cs4v2sv13crwxf0wf"))))
   (build-system font-build-system)
   (home-page "https://www.nerdfonts.com/")
   (synopsis "Modified version of FiraCode font")
   (description
    "This package provides a modified version of @code{font-fira-code}.")
   (license license:silofl1.1)))
