;;; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (gpodder)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix utils)
  #:use-module (gnu packages gpodder)
  #:use-module (gnu packages qt))

(define-public libmygpo-qt6-1.1.0-0.4d1f482
  (let ((commit "4d1f48291791c64f029e69138e3bc7fb6a851610")
        (revision "0"))
    (package/inherit libmygpo-qt
      (name "libmygpo-qt6")
      (version (git-version "1.1.0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri
          (git-reference
            (url "https://github.com/gpodder/libmygpo-qt")
            (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "1bxv8blyq66600i8skxmccb1lnmliz90378kck4f6j2ynry2114b"))
         (patches
          (list (local-file
                 "patches/libmygpo-qt-suppot-building-with-qt6.patch")))))
      (arguments
       (substitute-keyword-arguments (package-arguments libmygpo-qt)
         ((#:configure-flags flags)
          #~(append (list "-DBUILD_WITH_QT6=ON")
                    #$flags))))
      (inputs
       (modify-inputs (package-inputs libmygpo-qt)
         (replace "qtbase" qtbase))))))
