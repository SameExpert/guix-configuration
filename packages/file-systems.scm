;;; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (file-systems)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages disk)
  #:use-module (gnu packages pkg-config))

(define-public fatresize
  (package
    (name "fatresize")
    (version "1.1.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/ya-mouse/fatresize")
                    (commit (string-append "v" version))))
              (sha256
               (base32
                "1vhz84kxfyl0q7mkqn68nvzzly0a4xgzv76m6db0bk7xyczv1qr2"))))
    (build-system gnu-build-system)
    (native-inputs
     (list pkg-config))
    (inputs
     (list parted))
    (home-page "https://github.com/ya-mouse/fatresize")
    (synopsis "Tool to resize FAT partitions using libparted")
    (description
     "This package provides a tool to resize FAT partitions using libparted")
    (license license:gpl3+)))
