;;; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (gnome-xyz)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix build-system copy)
  #:use-module ((guix licenses) #:prefix license:))

(define-public beautyline-icon-theme
  (let ((commit "e13e5fa8f5aaad8a35fcaeb8a69512d32270a9d0")
       (revision "1"))
    (package
      (name "beautyline-icon-theme")
      (version (git-version "0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri
          (git-reference
            (url (string-append "https://gitlab.com/garuda-linux"
                                "/themes-and-settings/artwork/beautyline"))
            (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "0x80gfvp5j8mzwnd7ql5kpdxzcw37gdvicppga0sx4sjn9prlpmy"))))
      (build-system copy-build-system)
      (arguments
       (list #:install-plan
             #~`(("." "/share/icons/BeautyLine"
                  #:exclude ("AUTHORS"
                             "COPYING"
                             "README.md"
                             "ReadMe"
                             "filenames")))))
      (home-page (string-append "https://gitlab.com/garuda-linux"
                                "/themes-and-settings/artwork/beautyline"))
      (synopsis "Outlined icon theme")
      (description
       "BeautyLine is a set of outlined icons designed to have unified look and
comprehensive coverage.")
      (license license:gpl3+))))

(define-public beautysolar-icon-theme
  (package
    (name "beautysolar-icon-theme")
    (version "2")
    ;; Download "BeautySolar.tar-20241114161046.gz" from
    ;; https://www.pling.com/p/2037657.
    (source #f)
    (build-system copy-build-system)
    (arguments
     (list #:substitutable? #f
           #:install-plan
           #~`(("." "/share/icons/BeautySolar"
                #:exclude ("AUTHORS" "COPYING" "ReadMe")))))
    (home-page "https://www.pling.com/p/2037657")
    (synopsis "Icon theme with solar look and comprehensive coverage")
    (description
     "BeautySolar is Based on @code{BeautyLine Icons} icons, Designed to have
solar look and comprehensive coverage.")
    (license license:gpl3+)))

(define-public candy-icons-icon-theme
  (let ((commit "feb2e0b1b4ba09915867e50be662c582e5c83297")
          (revision "0"))
    (package
      (name "candy-icons-icon-theme")
      (version (git-version "0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri
          (git-reference
            (url "https://github.com/EliverLara/candy-icons")
            (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "1ailk91kcgmwa0nn8pdwyvpwb0ac7hwcnnz7h3qsrlzmfc4f7vv5"))))
      (build-system copy-build-system)
      (arguments
       (list #:install-plan
             #~`(("." "/share/icons/candy-icons"
                  #:exclude-regexp (".github" "LICENSE" "README.md")))))
      (home-page "https://github.com/EliverLara/candy-icons")
      (synopsis "Sweet gradient icons")
      (description
       "Candy icons is an icon theme colored with sweet gradients.")
      (license license:gpl3+))))

(define-public garuda-backgrounds
  (let ((commit "72b2e5abb134d3fe47bcb95a360d3d3b211f6b72")
        (revision "1"))
  (package
    (name "garuda-backgrounds")
    (version (git-version "0" revision commit))
    (source (origin
              (method git-fetch)
              (uri
               (git-reference
                  (url (string-append "https://gitlab.com/garuda-linux"
                                      "/themes-and-settings/artwork"
                                      "/garuda-backgrounds"))
                    (commit commit)))
              (sha256
               (base32
                "1b144mgj6458r76z1953yfs6xvbf0k1y8ycz22bqclgnj3y1978y"))))
    (build-system copy-build-system)
    (arguments
     (list #:substitutable? #f
           #:install-plan
           #~`(("src/" "share/backgrounds/"
                #:exclude ("garuda-backgrounds.xml"))
               ("src/garuda-backgrounds/garuda-backgrounds.xml"
                "share/gnome-background-properties/"))))
    (home-page (string-append "https://gitlab.com/garuda-linux"
                              "/themes-and-settings/artwork"
                              "/garuda-backgrounds"))
    (synopsis "Backgrounds from Garuda Linux")
    (description
     "This package provides backgrounds from Garuda Linux.")
    ;; License is mentioned in its PKGBUILD:
    ;; https://gitlab.com/garuda-linux/packages/stable-pkgbuilds/garuda-backgrounds/-/blob/main/PKGBUILD.
    (license license:gpl3+))))
