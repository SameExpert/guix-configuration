;;; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (kde-multimedia)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system qt)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages algebra)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages check)
  #:use-module (gnu packages crypto)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gstreamer)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages kde-frameworks)
  #:use-module (gnu packages kde-plasma)
  #:use-module (gnu packages libusb)
  #:use-module (gnu packages mp3)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages video)
  #:use-module (databases)
  #:use-module (gpodder)
  #:use-module (mp3))

(define-public amarok
  (package
    (name "amarok")
    (version "3.2.1")
    (source (origin
              (method url-fetch)
              (uri (string-append "mirror://kde/stable/amarok/" version
                                  "/amarok-" version ".tar.xz"))
              (sha256
               (base32
                "14r7iismx28nnh3jhkhczhrsmsap9by8p3kfhkp7d5678c7l62g4"))
              (patches
               (list
                (local-file
                 "patches/amarok-3.2.1-fix-including-mygpo-qt6.patch")))))
    (build-system qt-build-system)
    (arguments
     (list #:qtbase qtbase
           #:configure-flags
           #~(list "-DBUILD_WITH_QT6=ON")
           #:phases
           #~(modify-phases %standard-phases
               (add-before 'configure 'check-setup
                 (lambda _
                   ;; Set home directory.
                   (setenv "HOME" "/tmp")
                   ;; testplaylistlayout looks for "amarok/data" directory in
                   ;; $XDG_DATA_DIRS. Maybe it is for testing after installing.
                   ;; As a workaround, set XDG_DATA_DIRS pointing to $TMPDIR
                   ;; which contains "amarok/data" directory.
                   (let ((linktarget (string-append (dirname (getcwd))
                                                    "/amarok")))
                     (if (not (equal? (basename (getcwd)) "amarok"))
                       (symlink (getcwd) linktarget))
                     (setenv "XDG_DATA_DIRS"
                             (string-append (getenv "XDG_DATA_DIRS") ":"
                                            (dirname linktarget))))))
               (replace 'check
                 (lambda* (#:key tests? #:allow-other-keys)
                   (when tests?
                     ;; testsqlscanmanager fails, even when run manually.
                     (invoke "ctest" "-E" "testsqlscanmanager")))))))
    (native-inputs
       (list extra-cmake-modules googletest kdoctools pkg-config qttools))
    (inputs
     (list ffmpeg
           fftw
           glib
           karchive
           kcodecs
           kcolorscheme
           kconfig
           kconfigwidgets
           kcoreaddons
           kcmutils
           kcrash
           kdbusaddons
           kdnssd
           kglobalaccel
           kguiaddons
           ki18n
           kiconthemes
           kio
           kirigami
           knotifications
           kpackage
           kstatusnotifieritem
           ktexteditor
           ktextwidgets
           kwallet
           kwidgetsaddons
           kwindowsystem
           libofa
           libmtp
           libmygpo-qt6-1.1.0-0.4d1f482
           libxcrypt
           `(,mariadb-10.10.2 "dev")
           `(,mariadb-10.10.2 "lib")
           openssl
           phonon
           phonon-backend-vlc
           python
           qt5compat
           qtsvg
           qtwebengine
           solid
           taglib
           taglib-extras
           threadweaver))
    (home-page "https://amarok.kde.org/")
    (synopsis "Audio player for KDE")
    (description
     "Amarok is a music player and collection manager.  It features:
@itemize
@item dynamic playlists matching different criteria,
@item collection managing with rating support,
@item support for basic MTP and UMS music player devices,
@item integrated internet services such as Magnatune, Ampache and more,
@item scripting support,
@item cover manager and
@item replay gain support
@end itemize")
    (license license:gpl2+)))

(define-public rattlesnake
  (let ((commit "2f0631f201f16e1c17bcf82af24087370b799562")
         (revision "0"))
    (package
      (name "rattlesnake")
      (version (git-version "0" revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://invent.kde.org/multimedia/rattlesnake")
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32
                  "0iaandlbv2v4a6wsdxk19ynj77axd3kc72nw28bvbsfb0kqc0mai"))))
      (build-system qt-build-system)
      (arguments
       (list #:qtbase qtbase))
      (native-inputs
       (list extra-cmake-modules))
      (inputs
       (list kirigami qtdeclarative qtmultimedia))
      (propagated-inputs
       (list gstreamer gst-plugins-base gst-plugins-good))
      (home-page "https://invent.kde.org/multimedia/rattlesnake")
      (synopsis "Metronome")
      (description "Rattlesnake is a metronome app.")
      (license license:gpl3+))))
