;;; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (kde-xyz)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system qt)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (gnu packages fonts)
  #:use-module (gnu packages gnome-xyz)
  #:use-module (gnu packages kde-frameworks)
  #:use-module (gnu packages kde-plasma)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages xorg)
  #:use-module (fonts)
  #:use-module (gnome-xyz))

(define-public flat-remix-kde-theme
  (let ((commit "18ac464d5b77dd140aeb6c6b98d687c086959247")
        (revision "0"))
    (package
      (name "flat-remix-kde-theme")
      (version (git-version "0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri
          (git-reference
           (url "https://github.com/daniruiz/flat-remix-kde")
           (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "05wxcjpg3qgyc2jiidb8506s1ah7yhilb1ifk2xd61xmy7d1xmz6"))))
      (build-system copy-build-system)
      (arguments
       `(#:install-plan
         `(("." "/share"
            #:include-regexp ("/aurorae/" "/color-schemes/" "/plasma/")))))
      (propagated-inputs
       (list flat-remix-gtk-theme flat-remix-icon-theme))
      (home-page "https://drasite.com/flat-remix-kde")
      (synopsis "KDE  theme with material design")
      (description "Flat Remix KDE is a KDE theme inspired by material design.
It is mostly flat using a colorful palette with some shadows, highlights, and
gradients for some depth.")
    (license license:gpl3+))))

(define-public plasma-applet-advanced-radio-player
  ;; Version is not tagget, but given in package/metadata.desktop.
  (let ((commit "894973f4d5948ce400c2a3d28def4cea3c274c47") ;version 2.4
        (revision "0"))
    (package
      (name "plasma-applet-advanced-radio-player")
      (version (git-version "2.4" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri
          (git-reference
            (url "https://invent.kde.org/saurov/arp")
            (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "17h79bl6akyhjh53hdp7g4a7kki8v8m2zvqs1qi0isf3nlvz0dwm"))))
      (build-system copy-build-system)
      (arguments
       `(#:install-plan
         `(("package"
            "/share/plasma/plasmoids/org.kde.plasma.advancedradio"))))
      (propagated-inputs
       (list qtmultimedia-5))
      (home-page "https://invent.kde.org/saurov/arp")
      (synopsis "Radio player extension for Plasma")
      (description
       "Advanced Radio Player is a radio player extension for Plasma, with
editable list of stations.")
    (license license:lgpl2.0+))))

(define-public plasma-applet-better-inline-clock
  (let ((commit "bbad71b48073879f16b484788d01831ad53316b5") ;version 3.1
        (revision "0"))
    (package
      (name "plasma-applet-better-inline-clock")
      (version "3.1")
      (source
       (origin
         (method git-fetch)
         (uri
          (git-reference
            (url
             "https://github.com/MarianArlt/kde-plasmoid-betterinlineclock")
            (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "079k170dvga736hv4pi1n4mbqdwk1wl6n7x4blf22mn53zlcrl84"))))
      (build-system copy-build-system)
      (arguments
       `(#:install-plan
         `(("org.kde.plasma.betterinlineclock"
            "/share/plasma/plasmoids/org.kde.plasma.betterinlineclock"))))
      (home-page
       "https://github.com/MarianArlt/kde-plasmoid-betterinlineclock")
      (synopsis "Single line clock applet for Plasma")
      (description
       "This package provides single line clock applet for Plasma.")
      (license license:gpl2+))))

(define-public plasma-applet-window-appmenu
  ;; This commit fixes appmenu being unresponsive when hovering over other
  ;; options after clicking on one.
  (let ((commit "e044296256c866177c5c929f9280fb86b26dbf06")
        (revision "0"))
    (package
      (name "plasma-applet-window-appmenu")
      (version (git-version "0.8.0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri
          (git-reference
            (url "https://github.com/psifidotos/applet-window-appmenu")
            (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "0n8jgn6vaw4ncvrqm1snn6izg16by1f9rzkjypphr8a6z65nz2dn"))))
      (build-system qt-build-system)
      (native-inputs
       (list extra-cmake-modules))
      (inputs
       (list libsm
             kconfigwidgets
             kdecoration
             kirigami
             kitemmodels
             kwayland
             kwindowsystem
             plasma-framework
             plasma-workspace
             qtdeclarative-5
             qtx11extras))
      (home-page "https://github.com/psifidotos/applet-window-appmenu")
      (synopsis "Appmenu applet for Plasma")
      (description
       "This plasmoid shows the current window appmenu in Plasma panels or
Latte Dock.")
      (license license:gpl2+))))

(define-public plasma-applet-window-buttons
  (package
    (name "plasma-applet-window-buttons")
    (version "0.13.0")
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
          (url "https://github.com/moodyhunter/applet-window-buttons6")
          (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0wz6dhy4i1nb3a2hcvkq1m7b1wm09phssvqvzni3lh1qz865rcjb"))))
    (build-system qt-build-system)
    (arguments
     (list #:qtbase qtbase))
    (native-inputs
     (list extra-cmake-modules))
    (inputs
     (list kcmutils
           kconfigwidgets
           kcoreaddons
           kdeclarative
           kdecoration
           ki18n
           kpackage
           kservice
           ksvg
           libplasma))
    (propagated-inputs
     (list kdecoration))
    (home-page "https://github.com/moodyhunter/applet-window-buttons6")
    (synopsis "Window buttons plasmoid")
    (description
     "This plasmoid shows window buttons.")
    (license license:gpl2+)))

(define-public plasma-applet-window-title
  (package
    (name "plasma-applet-window-title")
    (version "0.21")
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
          (url "https://github.com/dhruv8sh/plasma6-window-title-applet")
          (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1vrm5v09gpnd0l1ik7hj2x1980z9x0qss70rw17vivvv5iidaqnq"))))
    (build-system copy-build-system)
    (arguments
     `(#:install-plan
       `(("." "/share/plasma/plasmoids/org.kde.windowtitle"
          #:include-regexp ("/contents/" "metadata.json")))))
    (inputs
     (list kdeclarative
           kirigami
           plasma-workspace))
    (home-page "https://github.com/dhruv8sh/plasma6-window-title-applet")
    (synopsis "Window title plasmoid")
    (description
     "This plasmoid shows the application title and the icon of the active
window.")
    (license license:gpl2+)))

(define-public plasma-wallpaper-active-blur
  (package
    (name "plasma-wallpaper-active-blur")
    (version "3.2.0")
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
          (url "https://github.com/bouteillerAlan/blurredwallpaper")
          (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1aggdkn8imm6dk6z2jng8515dgwyk7x6v7njw08svn15yy1ppwrz"))))
    (build-system copy-build-system)
    (arguments
     `(#:install-plan
       `(("." "/share/plasma/wallpapers/a2n.blur"
          #:include-regexp ("/assets/" "/contents/" "metadata.json")))))
    (home-page "https://github.com/bouteillerAlan/blurredwallpaper")
    (synopsis "Blurred wallpaper plugin for Plasma")
    (description
     "This Plasma wallpaper plugin blurs the wallpaper when a window is
active.")
    (license license:gpl2+)))

(define-public sweet-plasma-desktop-theme
 (let ((commit "fd0710a7045683434bf3cdf52a8c44400b28c365")
       (revision "0"))
   (package
     (name "sweet-plasma-desktop-theme")
     (version (git-version "0" revision commit))
     (source
      (origin
        (method git-fetch)
        (uri
         (git-reference
           (url "https://github.com/EliverLara/Sweet-kde")
           (commit commit)))
        (file-name (git-file-name name version))
        (sha256
         (base32 "05gih297xw2gg0vbcfwz5lchbv8jw9n9frnfpcmidw29jp1rjnd5"))))
     (build-system copy-build-system)
     (arguments
      (list #:install-plan
            #~`(("." "/share/plasma/desktoptheme/Sweet"
                 #:exclude-regexp (".github"
                                   "LICENSE"
                                   ".gitignore"
                                   "README.md")))))
     (home-page "https://github.com/EliverLara/Sweet-kde")
     (synopsis "Dark and modern desktop theme for Plasma shell")
     (description
      "This package provides a dark and modern desktop theme for Plasma.")
     (license license:cc-by4.0))))

(define-public sweet-nova-theme
  ;; Use the latest commit from nova branch.
  (let ((commit "99c77fcd1f09cf4e726ef1f30139b2355bed3c7e")
        (revision "0"))
    (package
      (name "sweet-nova-theme")
      (version (git-version "5.0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri
          (git-reference
            (url "https://github.com/EliverLara/Sweet")
            (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "0piwlp2haqlgcx7dnvhpq82yvg10hvgz7ynvpij0jijjr7hmsmfq"))))
      (build-system copy-build-system)
      (arguments
       (list #:install-plan
             #~`(("index.theme" "/share/themes/Sweet/")
                 ("assets" "/share/themes/Sweet/")
                 ("metacity-1" "/share/themes/Sweet/")
                 ("gtk-2.0" "/share/themes/Sweet/gtk-2.0")
                 ("gtk-3.0" "/share/themes/Sweet/gtk-3.0"
                  #:exclude-regexp ("\\.scss"))
                 ("gtk-4.0" "/share/themes/Sweet/gtk-4.0"
                  #:exclude-regexp ("\\.scss"))
                 ("cinnamon" "/share/themes/Sweet/cinnamon"
                  #:exclude-regexp ("\\.scss$"))
                 ("gnome-shell" "/share/themes/Sweet/gnome-shell"
                  #:exclude-regexp ("\\.scss$"))
                 ("xfwm4" "/share/themes/Sweet/")
                 ("kde/aurorae" "/share/aurorae/themes"
                  #:exclude-regexp ("\\.directory" "\\.shade\\.svg"))
                 ("kde/colorschemes" "/share/color-schemes")
                 ("kde/cursors/Sweet-cursors" "/share/icons/")
                 ("kde/look-and-feel" "/share/plasma/look-and-feel/Sweet")
                 ("kde/konsole" "/share/")
                 ("kde/Kvantum" "/share/")
                 ("kde/sddm" "/share/sddm/themes/Sweet")
                 ("extras/Sweet-Wallpapers" "/share/wallpapers"))))
      (propagated-inputs
       (list candy-icons-icon-theme sweet-plasma-desktop-theme))
      (home-page "https://github.com/EliverLara/Sweet/tree/nova")
      (synopsis "Colorful theme with wallpapers")
      (description
       "Sweet Nova is a set of colorful theme and wallpapers.

It contains:

@itemize
@item a Metacity, GTK 2, GTK 3 and a GTK 4 theme
@item a light and dark theme for Cinnamon, GNOME Shell, Plasma and Xfce,
@item a Kvantum theme with a transparent toolbar theme,
@item an SDDM theme,
@item cursors
@item and wallpapers.
@end itemize")
      (license license:gpl3+))))

(define-public garuda-wallpapers
  (let ((commit "046ab18044a5d703c86135acfc641dd7f625c8e9")
        (revision "1"))
    (package
      (name "garuda-wallpapers")
      (version (git-version "0" revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url (string-append "https://gitlab.com/garuda-linux"
                                          "/themes-and-settings/artwork"
                                          "/garuda-wallpapers"))
                      (commit commit)))
                (sha256
                 (base32
                  "1c1pl44qjcip5ic9q105qxgnrrhdvc18nx926bfiap2pshp42mdv"))))
      (build-system copy-build-system)
      (arguments
       (list #:substitutable? #f
             #:install-plan
             #~`(("src/" "share/wallpapers/"))))
      (home-page (string-append "https://gitlab.com/garuda-linux"
                                "/themes-and-settings/artwork"
                                "/garuda-wallpapers"))
      (synopsis "wallpapers from Garuda Linux")
      (description
       "This package provides wallpapers from Garuda Linux.")
      ;; License is mentioned in its PKGBUILD:
      ;; https://gitlab.com/garuda-linux/packages/stable-pkgbuilds/garuda-wallpapers/-/blob/main/PKGBUILD.
      (license license:gpl3+))))

(define-public ai-wallpapers
  (let ((commit "3706981bd889dbd46d976ff8f7bc2bbc6a009cdb")
        (revision "2"))
  (package
    (name "ai-wallpapers")
    (version (git-version "0" revision commit))
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
          (url "https://github.com/Schneegans/ai-wallpapers")
          (commit commit)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0iy0d0k0mdcfb6cbgwhgzgcsa6rz4zczxhqm7ipzqly55yraxbic"))))
    (build-system copy-build-system)
    (arguments
     (list #:substitutable? #f
           #:install-plan
           #~`(("." "share/wallpapers/ai-wallpapers"
                #:include-regexp (".*/.*/")))))
    (home-page "https://github.com/Schneegans/ai-wallpapers")
    (synopsis "AI-generated wallpapers")
    (description
     "This package contains wallpapers generated with Midjourney and upscaled
with Upscaler.")
    (license license:cc-by4.0))))

(define-public garuda-dr460nized-kde-theme
  (package
    (name "garuda-dr460nized-kde-theme")
    (version "4.5.4")
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
          (url (string-append "https://gitlab.com/garuda-linux/"
                              "themes-and-settings/settings/"
                              "garuda-dr460nized"))
          (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "07hyhvmvvkak3zfxrlwmay44gyhfdj4kfl5ibin0db3mzg04m9ri"))))
    (build-system copy-build-system)
    (arguments
     (list #:install-plan
           #~`(("etc" "etc"
                #:exclude-regexp ("autostart/"
                                  "environment.d/"
                                  "share/applications/"))
               ("usr/share" "share"
                #:exclude-regexp ("fastfetch" "wallpapers/" "README.md")))
           #:phases
           #~(modify-phases %standard-phases
               (add-after 'unpack 'patch-configurations
                 (lambda* (#:key inputs #:allow-other-keys)
                   (with-directory-excursion "etc/skel/"
                     (substitute* ".config/kscreenlockerrc"
                       (("\\/usr") (assoc-ref inputs "garuda-wallpapers")))
                     (substitute* ".local/share/konsole/Garuda.profile"
                       (("Command=.*$") "")))
                   (with-directory-excursion "usr/share"
                     (substitute* (string-append
                                    "plasma/look-and-feel/Dr460nized/contents/"
                                    "defaults")
                       (("\\/usr") (assoc-ref inputs "garuda-wallpapers")))
                     (substitute* (string-append
                                    "plasma/layout-templates/"
                                    "org.garuda.desktop.defaultDock/"
                                    "contents/layout.js")
                       (("applications:garuda-welcome\\.desktop(,|)") "")
                       (("applications:snapper-tools\\.desktop(,|)") "")
                       (("applications:octopi\\.desktop(,|)") ""))))))))
    (propagated-inputs
     (list beautyline-icon-theme
           fontconfig-google-noto-emoji
           font-fira-code-nerd
           font-fira-sans
           font-google-noto-emoji
           garuda-wallpapers
           kdeplasma-addons
           kvantum
           plasma-workspace
           plasma-applet-window-buttons
           plasma-applet-window-title
           plasma-wallpaper-active-blur
           sweet-nova-theme))
    (home-page (string-append "https://gitlab.com/garuda-linux"
                              "/themes-and-settings/settings"
                              "/garuda-dr460nized"))
    (synopsis "Garuda KDE Dr460nized themes and settings for Plasma shell")
    (description
     "This package provides themes and settings from Garuda KDE Dr460nized for
Plasma shell, some KDE applications like Kate and Konsole and for SDDM.")
    (license license:gpl3+)))
