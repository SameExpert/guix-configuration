;;; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (kde-utils)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system qt)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages image)
  #:use-module (gnu packages kde-frameworks)
  #:use-module (gnu packages mp3)
  #:use-module (gnu packages pdf)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages qt))

(define-public krename
  (let ((commit "00fc8b01cf4a9f3c52dc2191ce8572c1a870925c")
         (revision "0"))
    (package
      (name "krename")
      (version (git-version "5.0.2" revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://invent.kde.org/utilities/krename")
                      (commit commit)))
                (sha256
                 (base32
                  "1v06whz321n731p3cdlfwi2xvbyhxb0di7q77nlph3v27q85r2r2"))))
      (build-system qt-build-system)
      (arguments
       (list #:qtbase qtbase
              #:configure-flags
              #~(list "-DQT_MAJOR_VERSION=6")))
      (native-inputs
       (list extra-cmake-modules pkg-config))
      (inputs
       (list exiv2
             freetype
             karchive
             kcompletion
             kconfig
             kcoreaddons
             kcrash
             ki18n
             kiconthemes
             kio
             kitemviews
             kjobwidgets
             kjs
             kservice
             kwidgetsaddons
             kxmlgui
             podofo
             taglib
             qt5compat))
      (home-page "https://userbase.kde.org/KRename")
      (synopsis "Utility to handle specialized file renames")
      (description "KRename is a batch file renamer by KDE.  It allows you to
easily rename hundreds or even more files in one go.  The filenames can be
created by parts of the original filename, numbering the files or accessing
hundreds of information about the file, like creation date or Exif information
of an image.

Its features include:

@itemize
@item renaming a list of files based on a set of expressions,
@item copying/moving a list of files to another directory,
@item converting filenames to upper/lower case,
@item adding numbers to filenames,
@item finding and replacing parts of the filename,
@item rename Mp3/Ogg Vorbis files based on their ID3 tags,
@item setting access and modification dates, permissions and file ownership,
@item a plug-in API which allows you to extend KRename's features,
@item renaming directories recursively,
@item support for KFilePlugins,
@item creating undo file
@item and many more...
@end itemize")
      (license license:gpl3+))))
