;;; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (crates-io)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix download)
  #:use-module (guix build-system cargo)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages crates-io)
  #:use-module (gnu packages crates-windows)
  #:use-module (gnu packages rust-apps))

(define-public rust-goblin-0.6
  (package
    (inherit rust-goblin-0.7)
    (name "rust-goblin")
    (version "0.6.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "goblin" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0s7zs27b192virbp88y2fgq8p6nb8blkn7byqyl4cv7bm3j4ssqd"))))
    (arguments
     `(#:tests? #f          ; Not all files included.
       #:cargo-inputs
       (("rust-log" ,rust-log-0.4)
        ("rust-plain" ,rust-plain-0.2)
        ("rust-scroll" ,rust-scroll-0.11))))))

(define-public rust-reflink-0.1
  (package
    (name "rust-reflink")
    (version "0.1.3")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "reflink" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1glcyqvryv2zj6kjbfji0cldrkincqx3ds3wjwl4qnsnig15wn5w"))
       (patches
        (list
          (local-file
           "patches/rust-reflink-0.1-fix-64bit-toolchain-assumption.patch")))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs
       (("rust-libc" ,rust-libc-0.2)
        ("rust-winapi" ,rust-winapi-0.3))))
    (home-page "https://github.com/nicokoch/reflink")
    (synopsis "Copy-on-write mechanism on supported file systems")
    (description "This package provides copy-on-write mechanism on supported
file systems.")
    (license (list license:expat license:asl2.0))))

(define-public rust-rustyline-9.1.2
  (package/inherit rust-rustyline-9
    (name (package-name rust-rustyline-9))
    (version "9.1.2")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "rustyline" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0f8069ljhiv9nf97y975wvv9yvx82w3lm9g50d5n298fkiw2cy6v"))))
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs
       (("rust-bitflags" ,rust-bitflags-1)
       ("rust-cfg-if" ,rust-cfg-if-1)
       ("rust-clipboard-win" ,rust-clipboard-win-4)
       ("rust-dirs-next" ,rust-dirs-next-2)
       ("rust-fd-lock" ,rust-fd-lock-3)
       ("rust-libc" ,rust-libc-0.2)
       ("rust-log" ,rust-log-0.4)
       ("rust-memchr" ,rust-memchr-2)
       ("rust-nix" ,rust-nix-0.23)
       ("rust-radix-trie" ,rust-radix-trie-0.2)
       ("rust-regex" ,rust-regex-1)
       ("rust-scopeguard" ,rust-scopeguard-1)
       ("rust-smallvec" ,rust-smallvec-1)
       ("rust-unicode-segmentation" ,rust-unicode-segmentation-1)
       ("rust-unicode-width" ,rust-unicode-width-0.1)
       ("rust-utf8parse" ,rust-utf8parse-0.2)
       ("rust-winapi" ,rust-winapi-0.3)
       ("skim" ,skim))))))

(define-public rust-rustyline-derive-0.6
  (package
    (name "rust-rustyline-derive")
    (version "0.6.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "rustyline-derive" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0mw0nfi8xxsm4q80mv4va7ff8m0kgnsfjvv067zc1d8hp1daaddv"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs
       (("rust-quote" ,rust-quote-1)
        ("rust-syn" ,rust-syn-1))))
    (home-page "https://github.com/kkawakam/rustyline")
    (synopsis "Rustyline macros implementation in Rust")
    (description
     "This package provides Rustyline macros implementation in Rust.")
    (license license:expat)))

(define-public rust-ryu-0.2
  (package
    (inherit rust-ryu-1)
    (name "rust-ryu")
    (version "0.2.8")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "ryu" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "13wsi4408qxi9w44pdf5zfds4ym7np2070wkhg1g4j4dvi4rasmr"))))
    (arguments
     `(#:cargo-inputs
       (("rust-no-panic" ,rust-no-panic-0.1))
       #:cargo-development-inputs
       (("rust-num-cpus" ,rust-num-cpus-1)
        ("rust-rand" ,rust-rand-0.5))))))

(define-public rust-serde-jsonrc-0.1
  (package
    (name "rust-serde-jsonrc")
    (version "0.1.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "serde_jsonrc" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1a5q0ba7jhgfl63l16plgl7sx1k58l2caxf7z2j5l677rh5yk4dm"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f ;`Deserialize` and `Serialize` are defined multiple times
       #:cargo-inputs
       (("rust-indexmap" ,rust-indexmap-1)
        ("rust-itoa" ,rust-itoa-0.4)
        ("rust-ryu" ,rust-ryu-0.2)
        ("rust-serde" ,rust-serde-1))
       #:cargo-development-inputs
       (("rust-compiletest-rs" ,rust-compiletest-rs-0.3)
        ("rust-serde-bytes" ,rust-serde-bytes-0.10)
        ("rust-serde-derive" ,rust-serde-derive-1))))
    (home-page "https://github.com/serde-rs/json")
    (synopsis "JSON serialization file format")
    (description
     "This package provides a JSON serialization file format.")
    (license (list license:expat license:asl2.0))))
