;; Load these variables:
;; * use-nonfree-packages?
;; * use-nvidia-proprietary-driver?
;; * my-desktop-environment
(load "../system/options.scm")

(use-modules (ice-9 match))

(packages->manifest
 (append
   (match my-desktop-environment
     ("plasma"
      (map (compose list specification->package+output)
        "chess"
        "kde-games")))
   (list
     "0ad"
     "anarch"
     "frozen-bubble"
     "pinball"
     "supertux"
     "supertuxkart"
     "xonotic")))
