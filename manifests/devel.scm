;; Load these variables:
;; * use-nonfree-packages?
;; * use-nvidia-proprietary-driver?
;; * my-desktop-environment
(load "../system/options.scm")

(use-modules (ice-9 match))

(specifications->manifest
 (append
   (match my-desktop-environment
     ("plasma"
      (map (compose list specification->package+output)
        "kapptemplate"
        "kdevelop")))
   (list
     "autoconf"
     "automake"
     "gcc"
     "gettext-minimal"
     "help2man"
     "make"
     "pkg-config"
     "po4a"
     "texinfo")))
