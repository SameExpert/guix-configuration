;; This "manifest" file can be passed to 'guix package -m' to reproduce
;; the content of your profile.  This is "symbolic": it only specifies
;; package names.  To reproduce the exact same profile, you also need to
;; capture the channels being used, as returned by "guix describe".
;; See the "Replicating Guix" section in the manual.

(specifications->manifest
 (list
   "alacritty"
   "avizo"
   "curseradio"
   "fuzzel"
   "htop"
   "icecat"
   "icedove"
   "kodi-wayland"
   "imv"
   "light"
   "lynx"
   "mpv"
   "musikcube"
   "pulseaudio"
   "pamixer"
   "ranger"
   "swappy"
   "sway"
   "swaybg"
   "swayhide"
   "swayidle"
   "swaylock"
   "swayr"
   "waybar"
   "wl-clipboard"
   "wf-recorder"
   "wob"
   "xdg-utils"
   "xdg-desktop-portal-wlr"
   "zathura"
   "zathura-pdf-poppler"))
