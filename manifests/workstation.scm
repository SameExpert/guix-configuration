;; Load these variables:
;; * use-nonfree-packages?
;; * use-nvidia-proprietary-driver?
;; * my-desktop-environment
(load "../system/options.scm")

(use-modules (guix transformations)
             (ice-9 match))

(define with-patches
 (options->transformation
  '((with-patch . "yabridgectl=patches/yabridgectl-vst-paths.patch"))))

(packages->manifest
 (append
   (match my-desktop-environment
     ("plasma"
      (map (compose list specification->packages+output)
        "calligra"
        "ghostwriter"
        "kdenlive"
        "kolourpaint"
        "krita"
        "kwave"
        "tellico"))
     (_ '()))
   (map (compose list specification->package+output)
     "abiword"
     "amsynth"
     "ardour"
     "autotalent"
     "bchoppr"
     "bitwig-studio"
     "bjumblr"
     "blender"
     "bschaffl"
     "bsequencer"
     "bshapr"
     "denemo"
     "distrho-ports"
     "dpf-plugins"
     "drumkv1"
     "fabla"
     "frescobaldi"
     "gimp"
     "giada"
     "gnumeric"
     "gsequencer"
     "helm"
     "inkscape"
     "jack-keyboard"
     "lsp-plugins"
     "noise-repellent"
     "padthv1"
     "pd"
     "qjackctl"
     "qpwgraph"
     "qsynth"
     "qtractor"
     "reaper"
     "samplv1"
     "soundconverter"
     "synthv1"
     "tap-lv2"
     "vmpk"
     "x42-plugins"
     "yoshimi"
     "zplugins"
     "zrythm"
     "zynaddsubfx")
   (list
     (with-patches (specifications->package "yabridgectl")))))
