From 822c7bb54dc0cf618e7b284aef584481d880a48a Mon Sep 17 00:00:00 2001
From: Sughosha <sughosha@disroot.org>
Date: Fri, 29 Nov 2024 07:20:33 +0530
Subject: [PATCH] Install yabridge VST2 in $VST2_PATH and yabridge VST3 in
 $VST3_PATH if defined.

---
 tools/yabridgectl/src/config.rs | 50 +++++++++++++++++++++++++++++----
 1 file changed, 44 insertions(+), 6 deletions(-)

diff --git a/tools/yabridgectl/src/config.rs b/tools/yabridgectl/src/config.rs
index d948beff..99545df2 100644
--- a/tools/yabridgectl/src/config.rs
+++ b/tools/yabridgectl/src/config.rs
@@ -362,18 +362,56 @@ pub fn yabridgectl_directories() -> Result<BaseDirectories> {
 // TODO: Use `lazy_static` for these things. `$HOME` can technically change at runtime but
 //       realistically it won't.
 
-/// Get the path where bridged VST2 plugin files should be placed when using the centralized
-/// installation location setting. This is a subdirectory of `~/.vst` so we can easily clean up
-/// leftover files without interfering with other native plugins.
+/// Get the path where VST2 modules bridged by yabridgectl should be placed in. This is a
+/// subdirectory of `~/.vst` so we can easily clean up leftover files without interfering with
+/// other native plugins. Unless `$VST2_PATH` is set, in which case this is a subdirectory if the
+/// first direcotry in `$VST2_PATH`.
 pub fn yabridge_vst2_home() -> PathBuf {
-    Path::new(&env::var("HOME").expect("$HOME is not set")).join(YABRIDGE_VST2_HOME)
+    // Noone's going to be running yabridgectl on Windows, but this would need to be a semicolon
+    // there
+    #[cfg(unix)]
+    const PATH_SEPARATOR: char = ':';
+
+    let vst2_path = env::var("VST2_PATH").ok();
+    let vst2_path: Option<PathBuf> = vst2_path
+        .as_ref()
+        .map(|path| {
+            path.split_once(PATH_SEPARATOR)
+                .map(|(first_path, _)| first_path)
+                .unwrap_or(path)
+        })
+        .map(|path| Path::new(path).join(YABRIDGE_PREFIX));
+
+    // If `$VST2_PATH` is not set (which it probably won't be), then we'll fall back to `~/.vst`
+    vst2_path.unwrap_or_else(|| {
+        Path::new(&env::var("HOME").expect("$HOME is not set")).join(YABRIDGE_VST2_HOME)
+    })
 }
 
 /// Get the path where VST3 modules bridged by yabridgectl should be placed in. This is a
 /// subdirectory of `~/.vst3` so we can easily clean up leftover files without interfering with
-/// other native plugins.
+/// other native plugins. Unless `$VST3_PATH` is set, in which case this is a subdirectory if the
+/// first direcotry in `$VST3_PATH`.
 pub fn yabridge_vst3_home() -> PathBuf {
-    Path::new(&env::var("HOME").expect("$HOME is not set")).join(YABRIDGE_VST3_HOME)
+    // Noone's going to be running yabridgectl on Windows, but this would need to be a semicolon
+    // there
+    #[cfg(unix)]
+    const PATH_SEPARATOR: char = ':';
+
+    let vst3_path = env::var("VST3_PATH").ok();
+    let vst3_path: Option<PathBuf> = vst3_path
+        .as_ref()
+        .map(|path| {
+            path.split_once(PATH_SEPARATOR)
+                .map(|(first_path, _)| first_path)
+                .unwrap_or(path)
+        })
+        .map(|path| Path::new(path).join(YABRIDGE_PREFIX));
+
+    // If `$VST3_PATH` is not set (which it probably won't be), then we'll fall back to `~/.vst3`
+    vst3_path.unwrap_or_else(|| {
+        Path::new(&env::var("HOME").expect("$HOME is not set")).join(YABRIDGE_VST3_HOME)
+    })
 }
 
 /// Get the path where CLAP modules bridged by yabridgectl should be placed in. This is a
-- 
2.46.0

