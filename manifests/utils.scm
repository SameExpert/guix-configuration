;; Load these variables:
;; * use-nonfree-packages?
;; * use-nvidia-proprietary-driver?
;; * my-desktop-environment
(load "../system/options.scm")

(use-modules (ice-9 match))

(packages->manifest
 (append
   (match my-desktop-environment
     ("plasma"
      (map (compose list specification->package+output)
        "kid3"
        "okteta"))
     (_ '()))
   (map (compose list specification->package+output)
     "binutils"
     "disarchive"
     "dmde"
     "encfs"
     "f3"
     "file"
     "gdb"
     "gifsicle"
     "glib:bin"
     "glibc"
     "icoutils"
     "innoextract"
     "inxi"
     "ld-wrapper"
     "libva-utils"
     "lshw"
     "makeself-safeextract"
     "man-db"
     "mesa-utils"
     "patch"
     "patchelf"
     "sqlite"
     "testdisk"
     "unrar"
     "unzip"
     "vorbis-tools"
     "winetricks")))
