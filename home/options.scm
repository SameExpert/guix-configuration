;; This file should define these variables:
;; * my-home-dotfiles-packages
;; * my-home-packages
;; * my-home-variables
;; * my-home-bash-aliases

(define-module (options)
  #:use-module (guix gexp))

(define-public my-home-dotfiles-packages
 (list
   "chromium"
   "git"
   "nano"
   "zynaddsubfx"))

(define-public my-home-packages
 (list
   "eza"))

(define-public my-home-variables
 `(("KODI_DATA" . "$XDG_DATA_HOME/kodi")
   ("YABRIDGE_CLAP_HOME" . "$HOME/.local/lib/clap/yabridge")
   ("YABRIDGE_VST2_HOME" . "$HOME/.local/lib/vst/yabridge")
   ("YABRIDGE_VST3_HOME" . "$HOME/.local/lib/vst3/yabridge")))

(define-public my-home-bash-aliases
 '(("cp" . "cp -v")
   ("diff" . "diff --color=auto")
   ("egrep" . "egrep --color=auto")
   ("fgrep" . "fgrep --color=auto")
   ("grep" . "grep --color=auto")
   ("ip" . "ip --color")
   ("ln" . "ln --symbolic --relative --interactive --verbose")
   ("ls" . "eza --color=auto --group --icons --long")
   ("mv" . "mv --interactive --verbose")
   ("rm" . "rm --interactive=once")
   ("tree" . "eza --color=auto --group --icons --tree")))
