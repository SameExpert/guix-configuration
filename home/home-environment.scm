;; Load these variables:
;; * my-home-dotfiles-packages
;; * my-home-packages
;; * my-home-variables
;; * my-home-bash-aliases
(load "options.scm")

;; Load these variables:
;; * use-nonfree-packages?
;; * use-nonfree-nvidia-driver?
;; * my-desktop-environment
(load "../system/options.local.scm")

(define-module (home-environment)
  #:use-module (gnu)
  #:use-module (gnu home)
  #:use-module (gnu home services)
  #:use-module (gnu home services desktop)
  #:use-module (gnu home services dotfiles)
  #:use-module (gnu home services gnupg)
  #:use-module (gnu home services guix)
  #:use-module (gnu home services media)
  #:use-module (gnu home services shells)
  #:use-module (gnu home services sound)
  #:use-module (gnu home services xdg)
  #:use-module (gnu packages)
  #:use-module (gnu services)
  #:use-module (guix channels)
  #:use-module (ice-9 match)
  #:use-module (options))

(define-public %home-channels
 (append
   (list
     (channel
       (name 'nonguix)
       (url "https://gitlab.com/nonguix/nonguix")
       (introduction
         (make-channel-introduction
          "897c1a470da759236cc11798f4e0a5f7d4d59fbc"
         (openpgp-fingerprint
          "2A39 3FFF 68F4 EF7A 3D29  12AF 6F51 20A0 22FB B2D5"))))
    (channel
      (name 'guix-gaming-games)
      (url "https://gitlab.com/guix-gaming-channels/games")
      (introduction
       (make-channel-introduction
        "c23d64f1b8cc086659f8781b27ab6c7314c5cca5"
        (openpgp-fingerprint
         "50F3 3E2E 5B0C 3D90 0424  ABE8 9BDC F497 A4BB CC7F")))))
   %default-channels))

(home-environment
  (packages
   (map (compose list specification->package+output)
   (append my-home-packages
     (list
       "coreutils"
       "font-apple-color-emoji"
       "fontconfig-google-noto-emoji"
       "font-fira-code-nerd"
       "font-fira-sans"
       "font-google-noto"
       "font-google-noto-emoji"
       "font-google-noto-sans-cjk"
       "gawk"
       "git"
       "git:send-email"
       "gnupg"
       "hicolor-icon-theme"
       "man-db"
       "nano"
       "ncurses"
       "nss-certs"
       "pipewire"
       "sed"
       "unicode-emoji"
       "xdg-desktop-portal"))))
  (services
   (append
     (list
       (service home-bash-service-type
        (home-bash-configuration
          (guix-defaults? #f)
          (environment-variables
           '(("HISTFILE" . "$XDG_CACHE_HOME/bash-history")))
          (aliases my-home-bash-aliases)
          (bashrc `(,(local-file "dotfiles/bash/.bashrc" "bashrc")))))
       (service home-channels-service-type %home-channels)
       (service home-dbus-service-type)
       (service home-dotfiles-service-type
        (home-dotfiles-configuration
          (layout 'stow)
          (directories (list "dotfiles"))
          (packages my-home-dotfiles-packages)))
       (service home-gpg-agent-service-type
        (home-gpg-agent-configuration
          (pinentry-program
           (match my-desktop-environment
             ("gnome"
              (file-append
                (specification->package+output "pinentry-gnome3")
                "/bin/pinentry-gnome3"))
             ((or "lxqt" "plasma")
              (file-append
                (specification->package+output "pinentry-qt")
                "/bin/pinentry-qt"))
             ("sway"
              (file-append
                (specification->package+output "pinentry-bemenu")
                "/bin/pinentry-bemenu"))
             (_ '())))))
       (service home-pipewire-service-type)
       (simple-service 'home-extra-environment-variables
         home-environment-variables-service-type
         (append
           `(("GUIX_PACKAGE_PATH" .
             ,(string-append
                (dirname (dirname (current-filename)))
                "/packages"
                "${GUIX_PACKAGE_PATH:+:}$GUIX_PACKAGE_PATH"))
             ("LD_LIBRARY_PATH" .
              ,(string-append
                 "$HOME/.guix-home/profile/lib/pipewire-0.3/jack"
                 "${LD_LIBRARY_PATH:+:}$LD_LIBRARY_PATH"))
             ("EDITOR" . "nano")
             ("GTK2_RC_FILES" . "$XDG_CONFIG_HOME/gtk-2.0/gtkrc")
             ("GTK_RC_FILES" . "$XDG_CONFIG_HOME/gtk-1.0/gtkrc")
             ("WGETRC" . "$XDG_CONFIG_HOME/wgetrc")
             ("WINEPREFIX" . "$XDG_STATE_HOME/wine"))
           my-home-variables))
       (simple-service 'home-shell-profile home-shell-profile-service-type
        `(,(local-file "dotfiles/.profile" "shell-profile"))))
     %base-home-services)))
